package polines.monitoringtugasakhir.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import polines.monitoringtugasakhir.activities.HomeActivity;

/**
 * Created by sherryy on 4/3/17.
 */

public class NotificationIntentService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public NotificationIntentService() {
        super("notificationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable final Intent intent) {
        Handler intentHandler = new Handler(Looper.getMainLooper());

        switch (intent.getAction()) {
            case "pengumuman":
                intentHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        manager.cancel(intent.getIntExtra("ID", 0));
                        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                        intent.putExtra("FRAGMENT", 2);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                break;
            case "surat_masuk":
                intentHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        manager.cancel(intent.getIntExtra("ID", 0));
                        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                        intent.putExtra("FRAGMENT", 5);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                break;
            case "surat_tidak_mengajar":
                intentHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        manager.cancel(intent.getIntExtra("ID", 0));
                        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                        intent.putExtra("FRAGMENT", 6);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                break;
            case "home":
                intentHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        manager.cancel(intent.getIntExtra("ID", 0));
                        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                        intent.putExtra("FRAGMENT", 1);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                break;
        }
    }
}
