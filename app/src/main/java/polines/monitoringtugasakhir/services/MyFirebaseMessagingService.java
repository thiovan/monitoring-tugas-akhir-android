package polines.monitoringtugasakhir.services;

import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import polines.monitoringtugasakhir.helper.NotificationUtils;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.d(TAG, "key, " + key + " value " + value);
        }

        SharedPreferences prefs = getSharedPreferences("polines.monitoringtugasakhir_preferences", MODE_PRIVATE);
        boolean terimaNotifikasi = prefs.getBoolean("notifikasi", true);

        if (terimaNotifikasi){
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationUtils notificationUtils = new NotificationUtils(this);
            notificationUtils.show(remoteMessage.getData(), notificationManager);
        }

    }

}
