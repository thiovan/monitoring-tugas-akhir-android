package polines.monitoringtugasakhir.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;

import polines.monitoringtugasakhir.config.Config;
import polines.monitoringtugasakhir.entities.UserTable;

public class MyFirebaseSubscriptionService extends IntentService {

    public static final String TAG = MyFirebaseSubscriptionService.class.getSimpleName();

    public MyFirebaseSubscriptionService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try
        {
            // Resets Instance ID and revokes all tokens.
            FirebaseInstanceId.getInstance().deleteInstanceId();

            // Now manually call onTokenRefresh()
            String refreshedToken = FirebaseInstanceId.getInstance().getToken(Config.FIREBASE_SENDER_ID, "FCM");

            //Subscribe Topic
            UserTable user = UserTable.findById(UserTable.class, 1);

            String topic = "";
            switch (user.getTipe()) {
                case "Mahasiswa":
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_global");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_mahasiswa");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_" + String.valueOf(user.getId_user()));
                    topic = "monta_global, monta_mahasiswa, monta_" + String.valueOf(user.getId_user());
                    break;
                case "Dosen":
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_global");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_dosen");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_" + String.valueOf(user.getId_user()));
                    topic = "monta_global, monta_dosen, monta_" + String.valueOf(user.getId_user());
                    break;
                case "Admin":
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_global");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_admin");
                    FirebaseMessaging.getInstance().subscribeToTopic("monta_" + String.valueOf(user.getId_user()));
                    topic = "monta_global, monta_admin, monta_" + String.valueOf(user.getId_user());
                    break;
            }

            Log.d("Token", refreshedToken);

            storeRegIdInPref(refreshedToken, topic);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void storeRegIdInPref(String token, String topic) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("FIREBASE", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.putString("topic", topic);
        editor.apply();
    }
}
