package polines.monitoringtugasakhir.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class MyFirebaseDeleteTokenService extends IntentService {
    public static final String TAG = MyFirebaseDeleteTokenService.class.getSimpleName();

    public MyFirebaseDeleteTokenService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try
        {
            // Resets Instance ID and revokes all tokens.
            FirebaseInstanceId.getInstance().deleteInstanceId();

            SharedPreferences pref = getApplicationContext().getSharedPreferences("FIREBASE", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.apply();

            Log.d(TAG, "Instance ID Deleted");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
