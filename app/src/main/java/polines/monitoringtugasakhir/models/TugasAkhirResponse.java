package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TugasAkhirResponse {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public TugasAkhir data;

    public Boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public TugasAkhir getData() {
        return data;
    }
}
