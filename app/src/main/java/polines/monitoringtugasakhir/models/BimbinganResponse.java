package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BimbinganResponse {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Bimbingan data;

    public Boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Bimbingan getData() {
        return data;
    }
}
