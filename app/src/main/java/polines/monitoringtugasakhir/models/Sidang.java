package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sidang {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("tugas_akhir")
    @Expose
    public TugasAkhir tugasAkhir;
    @SerializedName("tanggal")
    @Expose
    public String tanggal;
    @SerializedName("jam")
    @Expose
    public String jam;
    @SerializedName("ruang")
    @Expose
    public String ruang;
    @SerializedName("sekretaris")
    @Expose
    public User sekretaris;
    @SerializedName("penguji_1")
    @Expose
    public User penguji1;
    @SerializedName("penguji_2")
    @Expose
    public User penguji2;
    @SerializedName("penguji_3")
    @Expose
    public User penguji3;
    @SerializedName("status")
    @Expose
    public String status;

    public Integer getId() {
        return id;
    }

    public TugasAkhir getTugasAkhir() {
        return tugasAkhir;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getJam() {
        return jam;
    }

    public String getRuang() {
        return ruang;
    }

    public User getSekretaris() {
        return sekretaris;
    }

    public User getPenguji1() {
        return penguji1;
    }

    public User getPenguji2() {
        return penguji2;
    }

    public User getPenguji3() {
        return penguji3;
    }

    public String getStatus() {
        return status;
    }

}
