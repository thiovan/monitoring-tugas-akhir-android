package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TugasAkhir {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("judul")
    @Expose
    public String judul;
    @SerializedName("penyusun_1")
    @Expose
    public User penyusun1;
    @SerializedName("penyusun_2")
    @Expose
    public User penyusun2;
    @SerializedName("penyusun_3")
    @Expose
    public User penyusun3;
    @SerializedName("pembimbing_1")
    @Expose
    public User pembimbing1;
    @SerializedName("pembimbing_2")
    @Expose
    public User pembimbing2;
    @SerializedName("proposal")
    @Expose
    public String proposal;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String created_at;
    @SerializedName("updated_at")
    @Expose
    public String updated_at;

    public Integer getId() {
        return id;
    }

    public String getJudul() {
        return judul;
    }

    public User getPenyusun1() {
        return penyusun1;
    }

    public User getPenyusun2() {
        return penyusun2;
    }

    public User getPenyusun3() {
        return penyusun3;
    }

    public User getPembimbing1() {
        return pembimbing1;
    }

    public User getPembimbing2() {
        return pembimbing2;
    }

    public String getProposal() {
        return proposal;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
