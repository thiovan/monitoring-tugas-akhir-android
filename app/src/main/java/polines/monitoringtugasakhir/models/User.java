package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("nomor_induk")
    @Expose
    public String nomorInduk;
    @SerializedName("nama")
    @Expose
    public String nama;
    @SerializedName("kelas")
    @Expose
    public String kelas;
    @SerializedName("tipe")
    @Expose
    public String tipe;
    @SerializedName("foto")
    @Expose
    public String foto;

    public Integer getId() {
        return id;
    }

    public String getNomorInduk() {
        return nomorInduk;
    }

    public String getNama() {
        return nama;
    }

    public String getKelas() {
        return kelas;
    }

    public String getTipe() {
        return tipe;
    }

    public String getFoto() {
        return foto;
    }

}
