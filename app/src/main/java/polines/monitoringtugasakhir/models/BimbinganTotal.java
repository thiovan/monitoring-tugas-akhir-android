package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BimbinganTotal {
    @SerializedName("total_bimbingan_1")
    @Expose
    public Integer totalBimbingan1;
    @SerializedName("total_bimbingan_2")
    @Expose
    public Integer totalBimbingan2;

    public Integer getTotalBimbingan1() {
        return totalBimbingan1;
    }

    public Integer getTotalBimbingan2() {
        return totalBimbingan2;
    }
}
