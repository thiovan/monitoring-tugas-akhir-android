package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bimbingan {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("mahasiswa")
    @Expose
    public User mahasiswa;
    @SerializedName("tugas_akhir")
    @Expose
    public TugasAkhir tugasAkhir;
    @SerializedName("pembimbing")
    @Expose
    public User pembimbing;
    @SerializedName("keterangan")
    @Expose
    public String keterangan;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    public Integer getId() {
        return id;
    }

    public User getMahasiswa() {
        return mahasiswa;
    }

    public TugasAkhir getTugasAkhir() {
        return tugasAkhir;
    }

    public User getPembimbing() {
        return pembimbing;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
