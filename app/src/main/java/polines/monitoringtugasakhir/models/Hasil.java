package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hasil {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("tugas_akhir")
    @Expose
    public TugasAkhir tugasAkhir;
    @SerializedName("mahasiswa")
    @Expose
    public User mahasiswa;
    @SerializedName("rata2_pembimbing")
    @Expose
    public String rata2Pembimbing;
    @SerializedName("rata2_penguji")
    @Expose
    public String rata2Penguji;
    @SerializedName("hasil")
    @Expose
    public String hasil;

    public Integer getId() {
        return id;
    }

    public TugasAkhir getTugasAkhir() {
        return tugasAkhir;
    }

    public User getMahasiswa() {
        return mahasiswa;
    }

    public String getRata2Pembimbing() {
        return rata2Pembimbing;
    }

    public String getRata2Penguji() {
        return rata2Penguji;
    }

    public String getHasil() {
        return hasil;
    }
}
