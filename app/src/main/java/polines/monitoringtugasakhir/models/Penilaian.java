package polines.monitoringtugasakhir.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Penilaian {
    @Expose
    public Integer id;
    @SerializedName("mahasiswa")
    @Expose
    public User mahasiswa;
    @SerializedName("penilai")
    @Expose
    public User penilai;
    @SerializedName("sebagai")
    @Expose
    public String sebagai;
    @SerializedName("nilai_1")
    @Expose
    public String nilai1;
    @SerializedName("nilai_2")
    @Expose
    public String nilai2;
    @SerializedName("nilai_3")
    @Expose
    public String nilai3;
    @SerializedName("nilai_4")
    @Expose
    public String nilai4;

    public Integer getId() {
        return id;
    }

    public User getMahasiswa() {
        return mahasiswa;
    }

    public User getPenilai() {
        return penilai;
    }

    public String getSebagai() {
        return sebagai;
    }

    public String getNilai1() {
        return nilai1;
    }

    public String getNilai2() {
        return nilai2;
    }

    public String getNilai3() {
        return nilai3;
    }

    public String getNilai4() {
        return nilai4;
    }

}
