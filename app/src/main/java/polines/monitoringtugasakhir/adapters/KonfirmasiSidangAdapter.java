package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.models.Sidang;
import polines.monitoringtugasakhir.models.TugasAkhir;

public class KonfirmasiSidangAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Sidang> modelList;

    private OnItemClickListener mItemClickListener;


    public KonfirmasiSidangAdapter(Context context, List<Sidang> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<Sidang> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_konfirmasi_sidang, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Sidang model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.judul.setText(model.getTugasAkhir().getJudul());

            String namaPenyusun;
            TugasAkhir tugasAkhir = model.getTugasAkhir();
            if (tugasAkhir.getPenyusun2() != null){
                namaPenyusun = "•" + tugasAkhir.getPenyusun1().getNama() + "\n•" + tugasAkhir.getPenyusun2().getNama();
            } else if (tugasAkhir.getPenyusun3() != null){
                namaPenyusun = "•" + tugasAkhir.getPenyusun1().getNama() + "\n•" + tugasAkhir.getPenyusun2().getNama() + "\n•" + tugasAkhir.getPenyusun3().getNama();
            } else {
                namaPenyusun = "•" + tugasAkhir.getPenyusun1().getNama();
            }
            genericViewHolder.penyusun.setText(namaPenyusun);

            String namaPembimbing = tugasAkhir.getPembimbing1().getNama() + "•\n" +
                    tugasAkhir.getPembimbing2().getNama() + "•";

            genericViewHolder.pembimbing.setText(namaPembimbing);
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Sidang getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Sidang model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView judul;
        private TextView penyusun;
        private TextView pembimbing;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.judul = (TextView) itemView.findViewById(R.id.judul);
            this.penyusun = (TextView) itemView.findViewById(R.id.penyusun);
            this.pembimbing = (TextView) itemView.findViewById(R.id.pembimbing);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }

}
