package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.models.TugasAkhir;

public class KonfirmasiBimbinganAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<TugasAkhir> modelList;
    private UserTable user;

    private KonfirmasiBimbinganAdapter.OnItemClickListener mItemClickListener;


    public KonfirmasiBimbinganAdapter(Context context, List<TugasAkhir> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<TugasAkhir> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public KonfirmasiBimbinganAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_tugas_akhir, viewGroup, false);

        return new KonfirmasiBimbinganAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof KonfirmasiBimbinganAdapter.ViewHolder) {
            final TugasAkhir model = getItem(position);
            KonfirmasiBimbinganAdapter.ViewHolder genericViewHolder = (KonfirmasiBimbinganAdapter.ViewHolder) holder;

            genericViewHolder.judul.setText(model.getJudul());

            String namaPenyusun;
            if (model.getPenyusun2() != null){
                namaPenyusun = "•" + model.getPenyusun1().getNama() + "\n•" + model.getPenyusun2().getNama();
            } else if (model.getPenyusun3() != null){
                namaPenyusun = "•" + model.getPenyusun1().getNama() + "\n•" + model.getPenyusun2().getNama() + "\n•" + model.getPenyusun3().getNama();
            } else {
                namaPenyusun = "•" + model.getPenyusun1().getNama();
            }
            genericViewHolder.penyusun.setText(namaPenyusun);

            if (model.getPembimbing1().getId().equals(user.getId_user())) {
                genericViewHolder.sebagai.setText("Pembimbing 1");
            } else if (model.getPembimbing2().getId().equals(user.getId_user())){
                genericViewHolder.sebagai.setText("Pembimbing 2");
            }

            String namaPembimbing = model.getPembimbing1().getNama() + "•\n" +
                    model.getPembimbing2().getNama() + "•";

            genericViewHolder.pembimbing.setText(namaPembimbing);
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final KonfirmasiBimbinganAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private TugasAkhir getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, TugasAkhir model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView judul;
        private TextView penyusun;
        private TextView sebagai;
        private TextView pembimbing;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.judul = (TextView) itemView.findViewById(R.id.judul);
            this.penyusun = (TextView) itemView.findViewById(R.id.penyusun);
            this.sebagai = (TextView) itemView.findViewById(R.id.sebagai);
            this.pembimbing = (TextView) itemView.findViewById(R.id.pembimbing);

            user = UserTable.findById(UserTable.class, 1);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }
}
