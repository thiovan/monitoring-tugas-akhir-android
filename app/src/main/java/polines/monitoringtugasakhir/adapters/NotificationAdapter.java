package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeUnits;
import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.Date;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.NotificationTable;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<NotificationTable> modelList;
    private OnItemClickListener mItemClickListener;

    public NotificationAdapter(Context context, List<NotificationTable> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<NotificationTable> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notifikasi, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final NotificationTable model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.title.setText(model.getTitle());
            genericViewHolder.message.setText(model.getMessage());

            Date date = DateTimeUtils.formatDate(Long.parseLong(model.getTimestamp()), DateTimeUnits.SECONDS);
            genericViewHolder.date.setText(DateTimeUtils.formatWithPattern(date, "EEEE, dd MMMM yyyy"));
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private NotificationTable getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, NotificationTable model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView message;
        private TextView date;
        private ImageView delete;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.title = (TextView) itemView.findViewById(R.id.title);
            this.message = (TextView) itemView.findViewById(R.id.message);
            this.date = (TextView) itemView.findViewById(R.id.date);
            this.delete = (ImageView) itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }

}
