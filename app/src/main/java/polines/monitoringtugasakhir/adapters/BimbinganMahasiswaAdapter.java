package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.models.Bimbingan;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class BimbinganMahasiswaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Bimbingan> modelList;
    private OnItemClickListener mItemClickListener;
    private int bimbingan1 = 1;
    private int bimbingan2 = 1;


    public BimbinganMahasiswaAdapter(Context context, List<Bimbingan> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(ArrayList<Bimbingan> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bimbingan_tugas_akhir, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Bimbingan model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            Date date = DateTimeUtils.formatDate(model.getUpdatedAt());
            genericViewHolder.tanggal.setText(DateTimeUtils.formatWithPattern(date, "dd/MM/yyyy"));

            if (model.getPembimbing().getId() == model.getTugasAkhir().getPembimbing1().getId()){
                genericViewHolder.bimbingan_ke.setText(String.format("Bimbingan %s", String.valueOf(bimbingan1)));
                genericViewHolder.line.setBackgroundColor(mContext.getResources().getColor(R.color.holo_orange_dark));
                bimbingan1++;
            }else{
                genericViewHolder.bimbingan_ke.setText(String.format("Bimbingan %s", String.valueOf(bimbingan2)));
                genericViewHolder.line.setBackgroundColor(mContext.getResources().getColor(R.color.holo_purple));
                bimbingan2++;
            }

            genericViewHolder.keterangan.setText(model.getKeterangan());
            genericViewHolder.pembimbingan.setText(model.getPembimbing().getNama());

            if (model.getStatus().equals("1")){
                genericViewHolder.bg_check.setBackgroundColor(mContext.getResources().getColor(R.color.lightGreen));
            }

        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Bimbingan getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Bimbingan model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView bimbingan_ke;
        private TextView keterangan;
        private TextView pembimbingan;
        private TextView tanggal;
        private View bg_check;
        private View line;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.bimbingan_ke = (TextView) itemView.findViewById(R.id.bimbingan_ke);
            this.keterangan = (TextView) itemView.findViewById(R.id.keterangan);
            this.pembimbingan = (TextView) itemView.findViewById(R.id.pembimbing);
            this.tanggal = (TextView) itemView.findViewById(R.id.tanggal);
            this.bg_check = (View) itemView.findViewById(R.id.bg_check);
            this.line = (View) itemView.findViewById(R.id.line);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }

}

