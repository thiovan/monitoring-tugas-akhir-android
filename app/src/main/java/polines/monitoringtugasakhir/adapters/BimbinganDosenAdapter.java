package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.thunder413.datetimeutils.DateTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.models.Bimbingan;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class BimbinganDosenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Bimbingan> modelList;
    private OnItemClickListener mItemClickListener;
    private int mahasiswa1 = 1;
    private int mahasiswa2 = 1;
    private int mahasiswa3 = 1;
    private String mahasiswaName1 = "";
    private String mahasiswaName2 = "";
    private String mahasiswaName3 = "";


    public BimbinganDosenAdapter(Context context, List<Bimbingan> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(ArrayList<Bimbingan> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bimbingan_tugas_akhir, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final Bimbingan model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

//            //membuat list mahasiswa
//            List<String> mahasiswaList = new ArrayList<>();
//            for (Bimbingan bimbingan : modelList) {
//                mahasiswaList.add(bimbingan.getMahasiswa().getNama());
//            }
//
//            //membuat set mahasiswa (remove duplicate value)
//            Set<String> mahasiswaSet = new HashSet<String>(mahasiswaList);
//
//            //convert set mahasiswa ke array string
//            String[] mahasiswaArray = mahasiswaSet.toArray(new String[mahasiswaSet.size()]);


            Date date = DateTimeUtils.formatDate(model.getUpdatedAt());
            genericViewHolder.tanggal.setText(DateTimeUtils.formatWithPattern(date, "dd/MM/yyyy"));

            Log.e("Mahasiswa", String.valueOf(model.getMahasiswa().getId() + "||" + String.valueOf(model.getTugasAkhir().getPenyusun1().getId())));

            if (model.getMahasiswa().getId() == model.getTugasAkhir().getPenyusun1().getId()){
                genericViewHolder.bimbingan_ke.setText(String.format("Bimbingan %s", String.valueOf(mahasiswa1)));
                genericViewHolder.line.setBackgroundColor(mContext.getResources().getColor(R.color.holo_orange_dark));
                mahasiswa1++;
            }else if(model.getMahasiswa().getId() == model.getTugasAkhir().getPenyusun2().getId()) {
                genericViewHolder.bimbingan_ke.setText(String.format("Bimbingan %s", String.valueOf(mahasiswa2)));
                genericViewHolder.line.setBackgroundColor(mContext.getResources().getColor(R.color.holo_purple));
                mahasiswa2++;
            }else if(model.getMahasiswa().getId() == model.getTugasAkhir().getPenyusun3().getId()) {
                genericViewHolder.bimbingan_ke.setText(String.format("Bimbingan %s", String.valueOf(mahasiswa3)));
                genericViewHolder.line.setBackgroundColor(mContext.getResources().getColor(R.color.holo_red_light));
                mahasiswa3++;
            }

            genericViewHolder.keterangan.setText(model.getKeterangan());
            genericViewHolder.pembimbingan.setText(model.getMahasiswa().getNama());

            if (model.getStatus().equals("1")){
                genericViewHolder.bg_check.setBackgroundColor(mContext.getResources().getColor(R.color.lightGreen));
            }

        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private Bimbingan getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, Bimbingan model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView bimbingan_ke;
        private TextView keterangan;
        private TextView pembimbingan;
        private TextView tanggal;
        private View bg_check;
        private View line;

        public ViewHolder(final View itemView) {
            super(itemView);

            this.bimbingan_ke = (TextView) itemView.findViewById(R.id.bimbingan_ke);
            this.keterangan = (TextView) itemView.findViewById(R.id.keterangan);
            this.pembimbingan = (TextView) itemView.findViewById(R.id.pembimbing);
            this.tanggal = (TextView) itemView.findViewById(R.id.tanggal);
            this.bg_check = (View) itemView.findViewById(R.id.bg_check);
            this.line = (View) itemView.findViewById(R.id.line);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }

}

