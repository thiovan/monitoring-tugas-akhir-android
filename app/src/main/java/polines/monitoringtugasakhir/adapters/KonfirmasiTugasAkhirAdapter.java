package polines.monitoringtugasakhir.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.WebViewActivity;
import polines.monitoringtugasakhir.models.TugasAkhir;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class KonfirmasiTugasAkhirAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<TugasAkhir> modelList;

    private OnItemClickListener mItemClickListener;


    public KonfirmasiTugasAkhirAdapter(Context context, List<TugasAkhir> modelList) {
        this.mContext = context;
        this.modelList = modelList;
    }

    public void updateList(List<TugasAkhir> modelList) {
        this.modelList = modelList;
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_konfirmasi_tugas_akhir, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final TugasAkhir model = getItem(position);
            ViewHolder genericViewHolder = (ViewHolder) holder;

            genericViewHolder.judul.setText(model.getJudul());

            String namaPenyusun;
            if (model.getPenyusun2() != null){
                namaPenyusun = "•" + model.getPenyusun1().getNama() + "\n•" + model.getPenyusun2().getNama();
            } else if (model.getPenyusun3() != null){
                namaPenyusun = "•" + model.getPenyusun1().getNama() + "\n•" + model.getPenyusun2().getNama() + "\n•" + model.getPenyusun3().getNama();
            } else {
                namaPenyusun = "•" + model.getPenyusun1().getNama();
            }

            genericViewHolder.penyusun.setText(namaPenyusun);

            String namaPembimbing = model.getPembimbing1().getNama() + "•\n" +
                                    model.getPembimbing2().getNama() + "•";

            genericViewHolder.pembimbing.setText(namaPembimbing);

            genericViewHolder.proposal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("URL", model.getProposal());
                    mContext.startActivity(intent);
                }
            });
        }
    }


    @Override
    public int getItemCount() {

        return modelList.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private TugasAkhir getItem(int position) {
        return modelList.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, TugasAkhir model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        
        private TextView judul;
        private TextView penyusun;
        private TextView pembimbing;
        private Button proposal;

        public ViewHolder(final View itemView) {
            super(itemView);
            
            this.judul = (TextView) itemView.findViewById(R.id.judul);
            this.penyusun = (TextView) itemView.findViewById(R.id.penyusun);
            this.pembimbing = (TextView) itemView.findViewById(R.id.pembimbing);
            this.proposal = (Button) itemView.findViewById(R.id.btn_show_proposal);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(itemView, getAdapterPosition(), modelList.get(getAdapterPosition()));


                }
            });

        }
    }

}

