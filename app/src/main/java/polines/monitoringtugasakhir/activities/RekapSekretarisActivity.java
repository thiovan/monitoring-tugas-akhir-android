package polines.monitoringtugasakhir.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.models.HasilResponse;
import polines.monitoringtugasakhir.models.Penilaian;
import polines.monitoringtugasakhir.models.PenilaianListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekapSekretarisActivity extends AppCompatActivity {

    private TextView mNama;
    private TextView mNilaiPembimbing1;
    private TextView mNilaiPembimbing2;
    private TextView mRataPembimbing;
    private TextView mNilaiPenguji1;
    private TextView mNilaiPenguji2;
    private TextView mNilaiPenguji3;
    private TextView mRataPenguji;
    private TextView mNilaiAkhir;
    private MaterialEditText mHasilAkhir;
    private Button mBtnSimpan;
    private FloatingActionButton mBtnRefresh;
    private  CreateToast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_sekretaris);
        initView();

        toast = new CreateToast(RekapSekretarisActivity.this);
        cekHasilSidang();
        getPenilaian(getIntent().getIntExtra("ID_MAHASISWA", 0));
        mNama.setText(getIntent().getStringExtra("NAMA_MAHASISWA"));

        mHasilAkhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListHasilAkhirDialog();
            }
        });
        mBtnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPenilaian(getIntent().getIntExtra("ID_MAHASISWA", 0));
            }
        });

        mBtnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mHasilAkhir.getText().toString().matches("")){
                    showConfirmDialog();
                }else{
                    Toasty.error(RekapSekretarisActivity.this, "Mohon Pilih Hasil Akhir Sidang", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initView() {
        mNama = (TextView) findViewById(R.id.nama);
        mNilaiPembimbing1 = (TextView) findViewById(R.id.nilai_pembimbing_1);
        mNilaiPembimbing2 = (TextView) findViewById(R.id.nilai_pembimbing_2);
        mRataPembimbing = (TextView) findViewById(R.id.rata_pembimbing);
        mNilaiPenguji1 = (TextView) findViewById(R.id.nilai_penguji_1);
        mNilaiPenguji2 = (TextView) findViewById(R.id.nilai_penguji_2);
        mNilaiPenguji3 = (TextView) findViewById(R.id.nilai_penguji_3);
        mRataPenguji = (TextView) findViewById(R.id.rata_penguji);
        mNilaiAkhir = (TextView) findViewById(R.id.nilai_akhir);
        mHasilAkhir = (MaterialEditText) findViewById(R.id.hasil_akhir);
        mBtnSimpan = (Button) findViewById(R.id.btn_simpan);
        mBtnRefresh = (FloatingActionButton) findViewById(R.id.btn_refresh);
    }

    private void getPenilaian(int id_mahasiswa){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Memuat data penilaian...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<PenilaianListResponse> userCall = service.showPenilaianMahasiswa(id_mahasiswa);
        userCall.enqueue(new Callback<PenilaianListResponse>() {
            @Override
            public void onResponse(Call<PenilaianListResponse> call, Response<PenilaianListResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    List<Penilaian> penilaians = response.body().getData();
                    for (Penilaian penilaian : penilaians) {
                        switch (penilaian.getSebagai()) {
                            case "Pembimbing 1":
                                Double nilai_pembimbing_1 = (Double.valueOf(penilaian.getNilai1())
                                        + Double.valueOf(penilaian.getNilai2())
                                        + Double.valueOf(penilaian.getNilai3())
                                        + Double.valueOf(penilaian.getNilai4()));
                                mNilaiPembimbing1.setText(String.format(Locale.US, "%.2f", nilai_pembimbing_1));
                                break;
                            case "Pembimbing 2":
                                Double nilai_pembimbing_2 = (Double.valueOf(penilaian.getNilai1())
                                        + Double.valueOf(penilaian.getNilai2())
                                        + Double.valueOf(penilaian.getNilai3())
                                        + Double.valueOf(penilaian.getNilai4()));
                                mNilaiPembimbing2.setText(String.format(Locale.US, "%.2f", nilai_pembimbing_2));
                                break;
                            case "Penguji 1":
                                Double nilai_penguji_1 = (Double.valueOf(penilaian.getNilai1())
                                        + Double.valueOf(penilaian.getNilai2())
                                        + Double.valueOf(penilaian.getNilai3())
                                        + Double.valueOf(penilaian.getNilai4()));
                                mNilaiPenguji1.setText(String.format(Locale.US, "%.2f", nilai_penguji_1));
                                break;
                            case "Penguji 2":
                                Double nilai_penguji_2 = (Double.valueOf(penilaian.getNilai1())
                                        + Double.valueOf(penilaian.getNilai2())
                                        + Double.valueOf(penilaian.getNilai3())
                                        + Double.valueOf(penilaian.getNilai4()));
                                mNilaiPenguji2.setText(String.format(Locale.US, "%.2f", nilai_penguji_2));
                                break;
                            case "Penguji 3":
                                Double nilai_penguji_3 = (Double.valueOf(penilaian.getNilai1())
                                        + Double.valueOf(penilaian.getNilai2())
                                        + Double.valueOf(penilaian.getNilai3())
                                        + Double.valueOf(penilaian.getNilai4()));
                                mNilaiPenguji3.setText(String.format(Locale.US, "%.2f", nilai_penguji_3));
                                break;
                            default:
                                break;
                        }
                    }

                    mRataPembimbing.setText(hitungRata(new String[]{
                            mNilaiPembimbing1.getText().toString(),
                            mNilaiPembimbing2.getText().toString()
                    }));

                    mRataPenguji.setText(hitungRata(new String[]{
                            mNilaiPenguji1.getText().toString(),
                            mNilaiPenguji2.getText().toString(),
                            mNilaiPenguji3.getText().toString()
                    }));

                    mNilaiAkhir.setText(hitungNilaiAkhir(new String[]{
                            mRataPembimbing.getText().toString(),
                            mRataPenguji.getText().toString()
                    }));

                }
            }

            @Override
            public void onFailure(Call<PenilaianListResponse> call, Throwable t) {
                loading.closeProgressDialog();
            }
        });
    }

    private void showListHasilAkhirDialog(){
        final String[] hasilAkhir = {"Lulus", "Lulus Dengan Revisi", "Tidak Lulus"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Pilih Hasil Akhir");
        dialog.setItems(hasilAkhir, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int i) {
                mHasilAkhir.setText(hasilAkhir[i]);
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private String hitungRata(String[] numbers){
        Double total = 0.0;
        for (int i=0; i<numbers.length; i++) {
            Double number = Double.valueOf(numbers[i]);
            total = total + number;
        }

        Double rata = total / numbers.length;
        return String.format(Locale.US, "%.2f", rata);
    }

    private String hitungNilaiAkhir(String[] numbers){
        Double total = 0.0;
        for (int i=0; i<numbers.length; i++) {
            Double number = Double.valueOf(numbers[i]);
            total = total + number;
        }

        return String.format(Locale.US, "%.2f", total);
    }

    private void showConfirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin menyimpan hasil rekap ini ?");
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                storeHasil();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void storeHasil(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Menyimpan hasil rekap...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<HasilResponse> userCall = service.storeHasil(
                String.valueOf(getIntent().getIntExtra("ID_TUGAS_AKHIR", 0)),
                String.valueOf(getIntent().getIntExtra("ID_MAHASISWA", 0)),
                mRataPembimbing.getText().toString(),
                mRataPenguji.getText().toString(),
                mHasilAkhir.getText().toString());

        userCall.enqueue(new Callback<HasilResponse>() {
            @Override
            public void onResponse(Call<HasilResponse> call, Response<HasilResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    finish();
                }else{
                    toast.internalServerError();
                }
            }

            @Override
            public void onFailure(Call<HasilResponse> call, Throwable t) {
                loading.closeProgressDialog();
                toast.onFailure();
            }
        });
    }

    private void cekHasilSidang(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Mengecek hasil penilaian mahasiswa...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<HasilResponse> userCall = service.getHasilByMahasiswa(getIntent().getIntExtra("ID_MAHASISWA", 0));

        userCall.enqueue(new Callback<HasilResponse>() {
            @Override
            public void onResponse(Call<HasilResponse> call, Response<HasilResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    AlertDialog alertDialog = new AlertDialog.Builder(RekapSekretarisActivity.this).create();
                    alertDialog.setCancelable(false);
                    alertDialog.setTitle("Informasi");
                    alertDialog.setMessage("Anda Sudah Mengisi Rekap Penilaian Mahasiswa Ini");
                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "TUTUP",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
                    alertDialog.show();

                    final Button centerButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                    LinearLayout.LayoutParams centerButtonLL = (LinearLayout.LayoutParams) centerButton.getLayoutParams();
                    centerButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    centerButton.setLayoutParams(centerButtonLL);
                }
            }

            @Override
            public void onFailure(Call<HasilResponse> call, Throwable t) {
               loading.closeProgressDialog();
            }
        });
    }
}
