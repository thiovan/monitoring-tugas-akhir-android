package polines.monitoringtugasakhir.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.NotificationAdapter;
import polines.monitoringtugasakhir.entities.NotificationTable;
import polines.monitoringtugasakhir.helper.CreateToast;

public class DaftarNotifikasiActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private NotificationAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_notifikasi);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Daftar Notifikasi");

        findViews();

        setAdapter();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                setAdapter();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });
    }

    private void findViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_recycler_list);
    }

    private void setAdapter() {
        List<NotificationTable> notificationTableList = NotificationTable.listAll(NotificationTable.class, "timestamp");

        if (notificationTableList.size() == 0){
            CreateToast toast = new CreateToast(this);
            toast.warning("Daftar Notifikasi Kosong");
        }

        mAdapter = new NotificationAdapter(this, notificationTableList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(mAdapter);

        mAdapter.SetOnItemClickListener(new NotificationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, NotificationTable model) {
                showConfirmDialog(model.getId());
            }
        });

    }

    private void showConfirmDialog(final long id_notification){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin menghapus notifikasi ini ?");
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                NotificationTable notificationTable = NotificationTable.findById(NotificationTable.class, id_notification);
                notificationTable.delete();
                setAdapter();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
