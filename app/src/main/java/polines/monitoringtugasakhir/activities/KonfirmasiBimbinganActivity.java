package polines.monitoringtugasakhir.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.BimbinganDosenAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.Bimbingan;
import polines.monitoringtugasakhir.models.BimbinganListResponse;
import polines.monitoringtugasakhir.models.BimbinganResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KonfirmasiBimbinganActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private FloatingActionButton floatingActionButton;
    private Spinner spinner;
    private BimbinganDosenAdapter mAdapter;
    private List<Bimbingan> modelList = new ArrayList<>();
    private List<Bimbingan> mahasiswa1List = new ArrayList<>();
    private List<Bimbingan> mahasiswa2List = new ArrayList<>();
    private List<Bimbingan> mahasiswa3List = new ArrayList<>();
    private UserTable user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_bimbingan);
        user = UserTable.findById(UserTable.class, 1);

        findViews();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Daftar Konfirmasi Bimbingan");

        getBimbingan(getIntent().getIntExtra("ID_TA", 0));
        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getBimbingan(getIntent().getIntExtra("ID_TA", 0));
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.darkBlue));
                if (i == 0){
                    setAdapter(mahasiswa1List);
                }else if (i == 1){
                    setAdapter(mahasiswa2List);
                }else if (i == 2){
                    setAdapter(mahasiswa3List);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void findViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_recycler_list);
        spinner = findViewById(R.id.spinner);
    }

    private void setAdapter(List<Bimbingan> mahasiswaList) {

        mAdapter = new BimbinganDosenAdapter(this, mahasiswaList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new BimbinganDosenAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Bimbingan model) {
                if (model.getStatus().equals("0")){
                    showConfirmDialog(model.getId());
                }
            }
        });

    }

    private void getBimbingan(int id_ta){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", getString(R.string.mengambil_data_bimbingan));
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<BimbinganListResponse> userCall = service.bimbinganByTA(id_ta, user.getId_user());

        userCall.enqueue(new Callback<BimbinganListResponse>() {
            @Override
            public void onResponse(Call<BimbinganListResponse> call, Response<BimbinganListResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()){
                        modelList = response.body().getData();
                        createSpinner();
                        createBimbinganList(modelList);
                    }
                }
            }

            @Override
            public void onFailure(Call<BimbinganListResponse> call, Throwable t) {
                loading.closeProgressDialog();
                Log.e("onFailure", t.getMessage());
                Toasty.error(KonfirmasiBimbinganActivity.this, getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createSpinner(){
        //membuat list mahasiswa
        List<String> mahasiswaList = new ArrayList<>();
        for (Bimbingan bimbingan : modelList) {
            mahasiswaList.add(bimbingan.getMahasiswa().getNama());
        }

        //membuat set mahasiswa (remove duplicate value)
        Set<String> mahasiswaSet = new HashSet<String>(mahasiswaList);

        //convert set mahasiswa ke array string
        String[] items = mahasiswaSet.toArray(new String[mahasiswaSet.size()]);

        //Set spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
    }

    private void createBimbinganList(List<Bimbingan> modelList){
        mahasiswa1List.clear();
        mahasiswa2List.clear();
        mahasiswa3List.clear();
        for (int i=0; i<modelList.size(); i++) {
            if (modelList.get(i).getMahasiswa().getId() == modelList.get(i).getTugasAkhir().getPenyusun1().getId()){
                mahasiswa1List.add(modelList.get(i));
            }else if(modelList.get(i).getMahasiswa().getId() == modelList.get(i).getTugasAkhir().getPenyusun2().getId()){
                mahasiswa2List.add(modelList.get(i));
            }else if(modelList.get(i).getMahasiswa().getId() == modelList.get(i).getTugasAkhir().getPenyusun3().getId()){
                mahasiswa3List.add(modelList.get(i));
            }
        }
    }

    private void showConfirmDialog(final Integer id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin konfirmasi bimbingan ini ?");
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                updateBimbingan(id, "1");
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updateBimbingan(int id, String status){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Menyimpan data bimbingan...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<BimbinganResponse> userCall = service.updateStatusBimbingan(id, status);

        userCall.enqueue(new Callback<BimbinganResponse>() {
            @Override
            public void onResponse(Call<BimbinganResponse> call, Response<BimbinganResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    getBimbingan(getIntent().getIntExtra("ID_TA", 0));
                }
            }

            @Override
            public void onFailure(Call<BimbinganResponse> call, Throwable t) {
                loading.closeProgressDialog();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
