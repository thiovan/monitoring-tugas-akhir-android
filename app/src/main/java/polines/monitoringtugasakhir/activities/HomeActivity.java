package polines.monitoringtugasakhir.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.NotificationTable;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.fragments.BimbinganTugasAkhirFragment;
import polines.monitoringtugasakhir.fragments.HasilSidangFragment;
import polines.monitoringtugasakhir.fragments.HomeFragment;
import polines.monitoringtugasakhir.fragments.KonfirmasiBimbinganFragment;
import polines.monitoringtugasakhir.fragments.KonfirmasiSidangFragment;
import polines.monitoringtugasakhir.fragments.KonfirmasiTugasAkhirFragment;
import polines.monitoringtugasakhir.fragments.ListTugaAkhirFragment;
import polines.monitoringtugasakhir.fragments.PendaftaranSidangFragment;
import polines.monitoringtugasakhir.fragments.PendaftaranTugasAkhirFragment;
import polines.monitoringtugasakhir.fragments.PengaturanFragment;
import polines.monitoringtugasakhir.fragments.PenilaianFragment;
import polines.monitoringtugasakhir.fragments.UndanganSidangFragment;
import polines.monitoringtugasakhir.services.MyFirebaseDeleteTokenService;
import polines.monitoringtugasakhir.services.MyFirebaseSubscriptionService;

public class HomeActivity extends AppCompatActivity {

    private IProfile profile;
    private Bundle bundle;
    private AccountHeader headerResult = null;
    private Drawer result = null;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageView notification;
    private TextView badge_notification;
    private UserTable user;
    private Fragment fragment = null;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        user = UserTable.findById(UserTable.class, 1);
        checkFirebaseSubscription();

        bundle = savedInstanceState;
        //initialize and create the image loader logic
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder, String tag) {
                Glide.with(imageView.getContext()).load(uri).placeholder(placeholder).centerCrop().into(imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Glide.clear(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx, String tag) {
                //define different placeholders for different imageView targets
                //default tags are accessible via the DrawerImageLoader.Tags
                //custom ones can be checked via string. see the CustomUrlBasePrimaryDrawerItem LINE 111
                if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
                    return DrawerUIUtils.getPlaceHolder(ctx);
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(com.mikepenz.materialdrawer.R.color.primary).sizeDp(56);
                } else if ("customUrlItem".equals(tag)) {
                    return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.md_red_500).sizeDp(56);
                }

                //we use the default one for
                //DrawerImageLoader.Tags.PROFILE_DRAWER_ITEM.name()

                return super.placeholder(ctx, tag);
            }
        });
        initNavDrawer();
        setSelectedFragment(user.getTipe(), 1);

        notification = (ImageView) findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, DaftarNotifikasiActivity.class);
                startActivity(intent);
            }
        });

        long notificationCount = NotificationTable.count(NotificationTable.class);
        badge_notification = (TextView) findViewById(R.id.badge_notification);
        badge_notification.setText(String.valueOf(notificationCount));
    }

    @Override
    protected void onResume() {
        super.onResume();
        long notificationCount = NotificationTable.count(NotificationTable.class);
        badge_notification.setText(String.valueOf(notificationCount));
    }

    public void initNavDrawer() {

        // Create profile
        String kelas = "";
        if (user.getTipe().equals("Mahasiswa")){
            kelas = " (" + user.getKelas() + ")";
        }

        profile = new ProfileDrawerItem().withName(user.getNama()).withEmail(user.getTipe() + kelas).withIcon(R.drawable.default_user_image);

        if (user.getTipe().equals("Admin")){
            NavigationDrawerAdmin(bundle, profile);
        } else if (user.getTipe().equals("Dosen") || user.getTipe().equals("Ketua Program Studi")){
            NavigationDrawerDosen(bundle, profile);
        } else {
            NavigationDrawerMahasiswa(bundle, profile);
        }
    }

    private void NavigationDrawerAdmin(Bundle savedInstanceState, IProfile profile) {
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withHeaderBackground(R.drawable.bg_toolbar)
                .addProfiles(
                        profile
                )
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                          return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();

        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_halaman_utama).withIcon(FontAwesome.Icon.faw_home).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_konfirmasi_tugas_akhir).withIcon(FontAwesome.Icon.faw_book).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_konfirmasi_bimbingan).withIcon(FontAwesome.Icon.faw_users).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_konfirmasi_sidang).withIcon(FontAwesome.Icon.faw_laptop).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_form_penilaian).withIcon(FontAwesome.Icon.faw_edit).withIdentifier(5),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_list_tugas_akhir).withIcon(FontAwesome.Icon.faw_book).withIdentifier(6),
                        new SectionDrawerItem().withName(R.string.section),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_pengaturan).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(91)
                )
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(FontAwesome.Icon.faw_sign_out_alt).withIdentifier(99)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Fragment fragment = null;
                        Long itemId = drawerItem.getIdentifier();

                        setSelectedFragment(user.getTipe(), itemId.intValue());

                        if (drawerItem instanceof Nameable) {

                        }

                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    private void NavigationDrawerDosen(Bundle savedInstanceState, IProfile profile) {
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withHeaderBackground(R.drawable.bg_toolbar)
                .addProfiles(
                        profile
                )
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();

        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_halaman_utama).withIcon(FontAwesome.Icon.faw_home).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_konfirmasi_bimbingan).withIcon(FontAwesome.Icon.faw_users).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_undangan_sidang).withIcon(FontAwesome.Icon.faw_calendar_alt2).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_form_penilaian).withIcon(FontAwesome.Icon.faw_edit2).withIdentifier(4),
                        new SectionDrawerItem().withName(R.string.section),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_pengaturan).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(91)
                )
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(FontAwesome.Icon.faw_sign_out_alt).withIdentifier(99)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Long itemId = drawerItem.getIdentifier();
                        setSelectedFragment(user.getTipe(), itemId.intValue());
                        if (drawerItem instanceof Nameable) {

                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    private void NavigationDrawerMahasiswa(Bundle savedInstanceState, IProfile profile) {
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(true)
                .withHeaderBackground(R.drawable.bg_toolbar)
                .addProfiles(
                        profile
                )
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();

        //Create the drawer
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_halaman_utama).withIcon(FontAwesome.Icon.faw_home).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_pendaftaran_tugas_akhir).withIcon(FontAwesome.Icon.faw_book).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_bimbingan_tugas_akhir).withIcon(FontAwesome.Icon.faw_users).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_pendaftaran_sidang).withIcon(FontAwesome.Icon.faw_graduation_cap).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_hasil_akhir_sidang).withIcon(FontAwesome.Icon.faw_star2).withIdentifier(5),
                        new SectionDrawerItem().withName(R.string.section),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_pengaturan).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(91)
                )
                .addStickyDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(FontAwesome.Icon.faw_sign_out_alt).withIdentifier(99)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Long itemId = drawerItem.getIdentifier();
                        setSelectedFragment(user.getTipe(), itemId.intValue());
                        if (drawerItem instanceof Nameable) {

                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    public void setSelectedFragment(String type, int identifier) {
        if (type.equals("Mahasiswa")) {
            //initializing the fragment object which is selected
            switch (identifier) {
                case 1:
                    fragment = new HomeFragment();
                    toolbarTitle.setText(R.string.drawer_item_halaman_utama);
                    break;
                case 2:
                    fragment = new PendaftaranTugasAkhirFragment();
                    toolbarTitle.setText(R.string.drawer_item_pendaftaran_tugas_akhir);
                    break;
                case 3:
                    fragment = new BimbinganTugasAkhirFragment();
                    toolbarTitle.setText(R.string.drawer_item_bimbingan_tugas_akhir);
                    break;
                case 4:
                    fragment = new PendaftaranSidangFragment();
                    toolbarTitle.setText(R.string.drawer_item_pendaftaran_sidang);
                    break;
                case 5:
                    fragment = new HasilSidangFragment();
                    toolbarTitle.setText(R.string.drawer_item_hasil_akhir_sidang);
                    break;
                case 91:
                    fragment = new PengaturanFragment();
                    toolbarTitle.setText(R.string.drawer_item_pengaturan);
                    break;
                case 99:
                    UserTable.deleteAll(UserTable.class);
                    NotificationTable.deleteAll(NotificationTable.class);
                    UserTable.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = 'USER_TABLE'");
                    Intent intentService = new Intent(getApplicationContext(), MyFirebaseDeleteTokenService.class);
                    startService(intentService);
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:

            }

        } else if (type.equals("Admin")) {
            //initializing the fragment object which is selected
            switch (identifier) {
                case 1:
                    fragment = new HomeFragment();
                    toolbarTitle.setText(R.string.drawer_item_halaman_utama);
                    break;
                case 2:
                    fragment = new KonfirmasiTugasAkhirFragment();
                    toolbarTitle.setText(R.string.drawer_item_konfirmasi_tugas_akhir);
                    break;
                case 3:
                    fragment = new KonfirmasiBimbinganFragment();
                    toolbarTitle.setText(R.string.drawer_item_konfirmasi_bimbingan);
                    break;
                case 4:
                    fragment = new KonfirmasiSidangFragment();
                    toolbarTitle.setText(R.string.drawer_item_konfirmasi_sidang);
                    break;
                case 5:
                    fragment = new PenilaianFragment();
                    toolbarTitle.setText(R.string.drawer_item_form_penilaian);
                    break;
                case 6:
                    fragment = new ListTugaAkhirFragment();
                    toolbarTitle.setText(R.string.drawer_item_list_tugas_akhir);
                    break;
                case 91:
                    fragment = new PengaturanFragment();
                    toolbarTitle.setText(R.string.drawer_item_pengaturan);
                    break;
                case 99:
                    UserTable.deleteAll(UserTable.class);
                    NotificationTable.deleteAll(NotificationTable.class);
                    UserTable.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = 'USER_TABLE'");
                    Intent intentService = new Intent(getApplicationContext(), MyFirebaseDeleteTokenService.class);
                    startService(intentService);
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:

            }

        } else if (type.equals("Dosen")) {
            switch (identifier) {
                case 1:
                    fragment = new HomeFragment();
                    toolbarTitle.setText(R.string.drawer_item_halaman_utama);
                    break;
                case 2:
                    fragment = new KonfirmasiBimbinganFragment();
                    toolbarTitle.setText(R.string.drawer_item_konfirmasi_bimbingan);
                    break;
                case 3:
                    fragment = new UndanganSidangFragment();
                    toolbarTitle.setText(R.string.drawer_item_undangan_sidang);
                    break;
                case 4:
                    fragment = new PenilaianFragment();
                    toolbarTitle.setText(R.string.drawer_item_form_penilaian);
                    break;
                case 91:
                    fragment = new PengaturanFragment();
                    toolbarTitle.setText(R.string.drawer_item_pengaturan);
                    break;
                case 99:
                    UserTable.deleteAll(UserTable.class);
                    NotificationTable.deleteAll(NotificationTable.class);
                    UserTable.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = 'USER_TABLE'");
                    Intent intentService = new Intent(getApplicationContext(), MyFirebaseDeleteTokenService.class);
                    startService(intentService);
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:
            }

        }

        //replacing the fragment
        if (fragment != null && identifier != 99) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment, String.valueOf(identifier));
            ft.commit();
        }

        result.setSelection(identifier, false);
    }

    @Override
    public void onBackPressed() {
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else if (result.getCurrentSelectedPosition() == 1) {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toasty.normal(this, "Tekan Kembali Sekali Lagi Untuk Keluar", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        } else {
            Fragment fragment = new HomeFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.commit();
            result.setSelection(1, false);
            toolbarTitle.setText(R.string.drawer_item_halaman_utama);
        }
    }

    private void checkFirebaseSubscription(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("FIREBASE", 0);
        String regId = pref.getString("regId", "");

        if (regId.equals("")){
            Intent intent = new Intent(getApplicationContext(), MyFirebaseSubscriptionService.class);
            startService(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // if you are using "FragmentPagerAdapter" then bellow two lines
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("2");
        if(fragment != null) fragment.onActivityResult(requestCode, resultCode, data);

    }
}
