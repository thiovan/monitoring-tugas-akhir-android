package polines.monitoringtugasakhir.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import polines.monitoringtugasakhir.R;


public class AuthorActivity extends AppCompatActivity {

    private ImageView mAuthor1Facebook;
    private ImageView mAuthor1Whatsapp;
    private ImageView mAuthor1Email;
    private ImageView mAuthor1Website;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);
        initView();

        mAuthor1Facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/thiovan99"));
                startActivity(intent);
            }
        });

        mAuthor1Whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:087700326227"));
                startActivity(intent);
            }
        });

        mAuthor1Email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "thiovan99@gmail.com", null));
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });

        mAuthor1Website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/thiovan"));
                startActivity(intent);
            }
        });

    }

    private void initView() {
        mAuthor1Facebook = (ImageView) findViewById(R.id.author1_facebook);
        mAuthor1Whatsapp = (ImageView) findViewById(R.id.author1_whatsapp);
        mAuthor1Email = (ImageView) findViewById(R.id.author1_email);
        mAuthor1Website = (ImageView) findViewById(R.id.author1_website);
    }
}
