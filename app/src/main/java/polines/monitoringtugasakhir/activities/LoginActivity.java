package polines.monitoringtugasakhir.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.dd.processbutton.iml.ActionProcessButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.helper.UserInput;
import polines.monitoringtugasakhir.models.UserResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private boolean LoginSuccess = false;
    private String LoginMessage = "Request Timeout, Mohon Login Kembali";
    private MaterialEditText mNomorInduk;
    private MaterialEditText mPassword;
    private ActionProcessButton mBtnLogin;
    private View[] userInputViews;
    private UserInput userInput;
    private CreateToast createToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        isLoggedIn();
        initView();
        userInputViews = new View[]{mNomorInduk, mPassword, mBtnLogin};
        userInput = new UserInput();
        createToast = new CreateToast(this);

        mBtnLogin.setMode(ActionProcessButton.Mode.ENDLESS);
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()){
                    //Disable Input
                    userInput.disable(userInputViews);

                    //Show Progress Button
                    mBtnLogin.setProgress(1);

                    //Request User User to Server
                    login(
                            mNomorInduk.getText().toString().replace(".", ""),
                            mPassword.getText().toString()
                    );

                }
            }
        });
    }

    private void initView() {
        mNomorInduk = (MaterialEditText) findViewById(R.id.nomor_induk);
        mPassword = (MaterialEditText) findViewById(R.id.password);
        mBtnLogin = (ActionProcessButton) findViewById(R.id.btnLogin);
    }

    private void isLoggedIn(){
        if (UserTable.count(UserTable.class) > 0){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public Boolean validation(){
        String snomorInduk = mNomorInduk.getText().toString();
        String spassword = mPassword.getText().toString();

        if (snomorInduk.matches("")){
            mNomorInduk.setError(getString(R.string.empty_nomor_induk));
            return false;
        }
        if (spassword.matches("")){
            mPassword.setError(getString(R.string.empty_password));
            return false;
        }

        return true;
    }

    private void login(String nomor_induk, String password) {
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserResponse> userCall = service.login(nomor_induk, password);

        userCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()){
                    UserResponse body = response.body();
                    if (body.getStatus()){

                        UserTable user = new UserTable(
                                body.getData().getId(),
                                body.getData().getNomorInduk(),
                                body.getData().getNama(),
                                body.getData().getKelas(),
                                body.getData().getTipe(),
                                body.getData().getFoto());
                        user.save();
                        mBtnLogin.setProgress(100);
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }else{

                        mBtnLogin.setProgress(-1);
                        createToast.error(body.getMessage());
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Stop Progress Button
                                mBtnLogin.setProgress(0);

                                //Enable Input
                                userInput.enable(userInputViews);
                            }
                        }, 2000);
                        mBtnLogin.setProgress(0);
                        userInput.enable(userInputViews);
                    }
                }else{

                    mBtnLogin.setProgress(0);
                    userInput.enable(userInputViews);
                    createToast.error(getString(R.string.internal_server_error));

                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                mBtnLogin.setProgress(0);
                userInput.enable(userInputViews);
                createToast.onFailure();
            }
        });
    }
}
