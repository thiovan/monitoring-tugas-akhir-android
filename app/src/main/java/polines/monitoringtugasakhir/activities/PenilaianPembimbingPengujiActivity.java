package polines.monitoringtugasakhir.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;

import java.util.Locale;

import es.dmoral.toasty.Toasty;
import io.feeeei.circleseekbar.CircleSeekBar;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.Penilaian;
import polines.monitoringtugasakhir.models.PenilaianResponse;
import polines.monitoringtugasakhir.models.User;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenilaianPembimbingPengujiActivity extends AppCompatActivity {

    private TextView mNama;
    private TextView mNilai1;
    private TextView mTotalNilai1;
    private ActionProcessButton mBtnNilai1;
    private TextView mNilai2;
    private TextView mTotalNilai2;
    private ActionProcessButton mBtnNilai2;
    private TextView mNilai3;
    private TextView mTotalNilai3;
    private ActionProcessButton mBtnNilai3;
    private TextView mNilai4;
    private TextView mTotalNilai4;
    private ActionProcessButton mBtnNilai4;
    private ActionProcessButton mBtnSimpan;
    private UserTable user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String sebagai = getIntent().getStringExtra("SEBAGAI");
        if (sebagai.equals("Pembimbing 1") || sebagai.equals("Pembimbing 2")){
            setContentView(R.layout.activity_penilaian_pembimbing);
        } else if (sebagai.equals("Penguji 1") || sebagai.equals("Penguji 2") || sebagai.equals("Penguji 3")){
            setContentView(R.layout.activity_penilaian_penguji);
        }

        user = UserTable.findById(UserTable.class, 1);

        initView();
        showPenilaian(getIntent().getIntExtra("ID_MAHASISWA", 0), user.getId_user());
        mNama.setText(getIntent().getStringExtra("NAMA_MAHASISWA"));

        mBtnNilai1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogInputNilai(mNilai1, mTotalNilai1, 0.10);
            }
        });

        mBtnNilai2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogInputNilai(mNilai2, mTotalNilai2, 0.15);
            }
        });

        mBtnNilai3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogInputNilai(mNilai3, mTotalNilai3, 0.20);
            }
        });

        mBtnNilai4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogInputNilai(mNilai4, mTotalNilai4, 0.05);
            }
        });

        mBtnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmDialog();
            }
        });
    }

    private void initView() {
        mNama = (TextView) findViewById(R.id.nama);
        mNilai1 = (TextView) findViewById(R.id.nilai_1);
        mTotalNilai1 = (TextView) findViewById(R.id.total_nilai_1);
        mBtnNilai1 = (ActionProcessButton) findViewById(R.id.btn_nilai_1);
        mNilai2 = (TextView) findViewById(R.id.nilai_2);
        mTotalNilai2 = (TextView) findViewById(R.id.total_nilai_2);
        mBtnNilai2 = (ActionProcessButton) findViewById(R.id.btn_nilai_2);
        mNilai3 = (TextView) findViewById(R.id.nilai_3);
        mTotalNilai3 = (TextView) findViewById(R.id.total_nilai_3);
        mBtnNilai3 = (ActionProcessButton) findViewById(R.id.btn_nilai_3);
        mNilai4 = (TextView) findViewById(R.id.nilai_4);
        mTotalNilai4 = (TextView) findViewById(R.id.total_nilai_4);
        mBtnNilai4 = (ActionProcessButton) findViewById(R.id.btn_nilai_4);
        mBtnSimpan = (ActionProcessButton) findViewById(R.id.btn_simpan);
    }

    private void showPenilaian(int id_mahasiswa, int id_penilai) {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Memuat data penilaian...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<PenilaianResponse> userCall = service.showPenilaian(id_mahasiswa, id_penilai);
        userCall.enqueue(new Callback<PenilaianResponse>() {
            @Override
            public void onResponse(Call<PenilaianResponse> call, Response<PenilaianResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()){
                        Penilaian penilaian = response.body().getData();
                        User mahasiswa = response.body().getData().getMahasiswa();

//                        mNilai1.setText(String.format(Locale.US, "%.2f",Double.valueOf(penilaian.getNilai1()) / 0.10));
//                        mTotalNilai1.setText(penilaian.getNilai1());
//                        mNilai2.setText(String.format(Locale.US, "%.2f",Double.valueOf(penilaian.getNilai2()) / 0.15));
//                        mTotalNilai2.setText(penilaian.getNilai2());
//                        mNilai3.setText(String.format(Locale.US, "%.2f",Double.valueOf(penilaian.getNilai3()) / 0.20));
//                        mTotalNilai3.setText(penilaian.getNilai3());
//                        mNilai4.setText(String.format(Locale.US, "%.2f",Double.valueOf(penilaian.getNilai4()) / 0.05));
//                        mTotalNilai4.setText(penilaian.getNilai4());

                        AlertDialog alertDialog = new AlertDialog.Builder(PenilaianPembimbingPengujiActivity.this).create();
                        alertDialog.setCancelable(false);
                        alertDialog.setTitle("Informasi");
                        alertDialog.setMessage("Anda Sudah Mengisi Form Penilaian Ini");
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "TUTUP",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                });
                        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
                        alertDialog.show();

                        final Button centerButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
                        LinearLayout.LayoutParams centerButtonLL = (LinearLayout.LayoutParams) centerButton.getLayoutParams();
                        centerButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        centerButton.setLayoutParams(centerButtonLL);
                    }
                }
            }

            @Override
            public void onFailure(Call<PenilaianResponse> call, Throwable t) {
                loading.closeProgressDialog();
            }
        });
    }

    private void dialogInputNilai(final TextView mNilai, final TextView mTotal_nilai, final double bobot){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_input_nilai);

        final CircleSeekBar seekBar = (CircleSeekBar) dialog.findViewById(R.id.seekbar);
        final TextView nilai = (TextView) dialog.findViewById(R.id.nilai);
        ActionProcessButton btnOk = (ActionProcessButton) dialog.findViewById(R.id.btn_ok);

        seekBar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar circleSeekBar, int i) {
                nilai.setText(String.valueOf(i));
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNilai.setText(nilai.getText().toString());
                mTotal_nilai.setText(String.format(Locale.US, "%.2f",Integer.parseInt(nilai.getText().toString()) * bobot));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void storeNilai(String mahasiswa, String penilai, String sebagai, String nilai_1, String nilai_2, String nilai_3, String nilai_4){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(this, "", "Menyimpan data penilaian...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<PenilaianResponse> userCall = service.storePenilaian(mahasiswa, penilai, sebagai, nilai_1, nilai_2, nilai_3, nilai_4);
        userCall.enqueue(new Callback<PenilaianResponse>() {
            @Override
            public void onResponse(Call<PenilaianResponse> call, Response<PenilaianResponse> response) {
                loading.closeProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()){
                        Toasty.success(PenilaianPembimbingPengujiActivity.this, "Nilai Berhasil Disimpan", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<PenilaianResponse> call, Throwable t) {
                loading.closeProgressDialog();
            }
        });
    }

    private void showConfirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin menyimpan penilaian ini ?");
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                storeNilai(
                        String.valueOf(getIntent().getIntExtra("ID_MAHASISWA", 0)),
                        String.valueOf(user.getId_user()),
                        getIntent().getStringExtra("SEBAGAI"),
                        mTotalNilai1.getText().toString(),
                        mTotalNilai2.getText().toString(),
                        mTotalNilai3.getText().toString(),
                        mTotalNilai4.getText().toString()
                );
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
