package polines.monitoringtugasakhir.network;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import polines.monitoringtugasakhir.models.BimbinganListResponse;
import polines.monitoringtugasakhir.models.BimbinganResponse;
import polines.monitoringtugasakhir.models.BimbinganTotalResponse;
import polines.monitoringtugasakhir.models.HasilResponse;
import polines.monitoringtugasakhir.models.PenilaianListResponse;
import polines.monitoringtugasakhir.models.PenilaianResponse;
import polines.monitoringtugasakhir.models.SidangListResponse;
import polines.monitoringtugasakhir.models.SidangResponse;
import polines.monitoringtugasakhir.models.TugasAkhirListResponse;
import polines.monitoringtugasakhir.models.TugasAkhirResponse;
import polines.monitoringtugasakhir.models.UserListResponse;
import polines.monitoringtugasakhir.models.UserResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Thio Van on 18/02/2018.
 */

public interface APIInterface {

    //User API
    @FormUrlEncoded
    @POST("user/login")
    Call<UserResponse> login(
            @Field("nomor_induk") String nomor_induk,
            @Field("password") String password);

    @GET("user/pembimbing")
    Call<UserListResponse> getPembimbingList();

    @GET("user/mahasiswa")
    Call<UserListResponse> getMahasiswaList();

    @GET("user/pembimbing/{id_user}")
    Call<UserListResponse> getPembimbingListByMahasiswa(@Path("id_user") int id_user);


    //TA API
    @Multipart
    @POST("ta")
    Call<TugasAkhirResponse> storeTugasAkhir(
            @Part("judul") RequestBody judul,
            @Part("penyusun_1") RequestBody penyusun_1,
            @Part("penyusun_2") RequestBody penyusun_2,
            @Part("penyusun_3") RequestBody penyusun_3,
            @Part("pembimbing_1") RequestBody pembimbing_1,
            @Part("pembimbing_2") RequestBody pembimbing_2,
            @Part("status") RequestBody status,
            @Part MultipartBody.Part file,
            @Part("name") RequestBody name);

    @FormUrlEncoded
    @POST("ta/{id}")
    Call<TugasAkhirResponse> updateTugasAkhir(
            @Path("id") int id,
            @Field("pembimbing_1") String pembimbing_1,
            @Field("pembimbing_2") String pembimbing_2,
            @Field("status") String status);

    @GET("ta")
    Call<TugasAkhirListResponse> getAllTA();

    @GET("ta/cek/{id_user}")
    Call<TugasAkhirResponse> cekStatusTA(@Path("id_user") int id_user);

    @GET("ta/konfirmasi")
    Call<TugasAkhirListResponse> getDaftarKonfirmasiTA();

    @GET("ta/bimbingan/{id_user}")
    Call<TugasAkhirListResponse> getBimbinganTugasAkhir(@Path("id_user") int id_user);


    //Bimbingan API
    @FormUrlEncoded
    @POST("bimbingan")
    Call<BimbinganResponse> storeBimbingan(
            @Field("mahasiswa") String mahasiswa,
            @Field("pembimbing") String pembimbing,
            @Field("keterangan") String keterangan);

    @FormUrlEncoded
    @POST("bimbingan/status/{id}")
    Call<BimbinganResponse> updateStatusBimbingan(
            @Path("id") int id,
            @Field("status") String status);

    @GET("bimbingan/mahasiswa/{id_user}")
    Call<BimbinganListResponse> bimbinganByMahasiswa(@Path("id_user") int id_user);

    @GET("bimbingan/cek/{id_user}")
    Call<BimbinganTotalResponse> getTotalBimbingan(@Path("id_user") int id_user);

    @GET("bimbingan/ta/{id_ta}/{id_pembimbing}")
    Call<BimbinganListResponse> bimbinganByTA(
            @Path("id_ta") int id_ta,
            @Path("id_pembimbing") int id_pembimbing
    );


    //Sidang API
    @FormUrlEncoded
    @POST("sidang")
    Call<SidangResponse> storeSidang(
            @Field("mahasiswa") String mahasiswa);

    @FormUrlEncoded
    @POST("sidang/{id}")
    Call<SidangResponse> updateSidang(
            @Path("id") int id,
            @Field("tanggal") String tanggal,
            @Field("jam") String jam,
            @Field("ruang") String ruang,
            @Field("sekretaris") String sekretaris,
            @Field("penguji_1") String penguji_1,
            @Field("penguji_2") String penguji_2,
            @Field("penguji_3") String penguji_3,
            @Field("status") String status
    );

    @GET("sidang/cek/{id_user}")
    Call<SidangResponse> cekStatusSidang(@Path("id_user") int id_user);

    @GET("sidang/konfirmasi")
    Call<SidangListResponse> getKonfirmasiSidang();

    @GET("sidang/dosen/{id_user}")
    Call<SidangListResponse> getPenilaian(@Path("id_user") int id_user);


    //Penilaian API
    @FormUrlEncoded
    @POST("penilaian")
    Call<PenilaianResponse> storePenilaian(
            @Field("mahasiswa") String mahasiswa,
            @Field("penilai") String penilai,
            @Field("sebagai") String sebagai,
            @Field("nilai_1") String nilai_1,
            @Field("nilai_2") String nilai_2,
            @Field("nilai_3") String nilai_3,
            @Field("nilai_4") String nilai_4
    );

    @GET("penilaian/show/{id_mahasiswa}/{id_penilai}")
    Call<PenilaianResponse> showPenilaian(@Path("id_mahasiswa") int id_mahasiswa,
                                      @Path("id_penilai") int id_penilai);

    @GET("penilaian/show/{id_mahasiswa}")
    Call<PenilaianListResponse> showPenilaianMahasiswa(@Path("id_mahasiswa") int id_mahasiswa);


    //Hasil API
    @FormUrlEncoded
    @POST("hasil")
    Call<HasilResponse> storeHasil(
            @Field("tugas_akhir") String tugas_akhir,
            @Field("mahasiswa") String mahasiswa,
            @Field("rata2_pembimbing") String rata2_pembimbing,
            @Field("rata2_penguji") String rata2_penguji,
            @Field("hasil") String hasil
    );

    @GET("hasil/{id_mahasiswa}")
    Call<HasilResponse> getHasilByMahasiswa(@Path("id_mahasiswa") int id_mahasiswa);
}
