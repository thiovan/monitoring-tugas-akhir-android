package polines.monitoringtugasakhir.config;

public class Config {

    public static final String BASE_URL = "http://monta.informatikapolines.com/";

    public static final String API_BASE_URL = BASE_URL + "api/";

    public static final String PROPOSAL_URL_DIRECTORY = BASE_URL + "laravel/public/proposal/";

    public static final String PDF_WRAPPER = "https://docs.google.com/gview?embedded=true&url=";

    public static final String APP_UPDATE_URL = "https://drive.google.com/open?id=1UKNio1TOhrKHGOFg6tGyGPQAv37alHcy";

    public static final String FIREBASE_SENDER_ID = "850316189359";
}
