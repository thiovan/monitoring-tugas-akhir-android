package polines.monitoringtugasakhir.entities;

import com.orm.SugarRecord;

public class UserTable extends SugarRecord {

    public Integer id_user;
    public String nomorInduk;
    public String nama;
    public String kelas;
    public String tipe;
    public String foto;

    public UserTable() {
    }

    public UserTable(Integer id_user, String nomorInduk, String nama, String kelas, String tipe, String foto) {
        this.id_user = id_user;
        this.nomorInduk = nomorInduk;
        this.nama = nama;
        this.kelas = kelas;
        this.tipe = tipe;
        this.foto = foto;
    }

    public Integer getId_user() {
        return id_user;
    }

    public void setId_user(Integer id_user) {
        this.id_user = id_user;
    }

    public String getNomorInduk() {
        return nomorInduk;
    }

    public void setNomorInduk(String nomorInduk) {
        this.nomorInduk = nomorInduk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
