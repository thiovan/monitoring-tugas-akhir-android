package polines.monitoringtugasakhir.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.HasilResponse;
import polines.monitoringtugasakhir.models.SidangResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HasilSidangFragment extends Fragment {


    private PieView mPieView;
    private TextView mHasilAkhir;
    private UserTable user;

    public HasilSidangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hasil_sidang, container, false);;
        initView(view);

        user = UserTable.findById(UserTable.class, 1);

        cekStatusSidang();

        mPieView.setPercentageBackgroundColor(getResources().getColor(R.color.colorAccent));
        mPieView.setInnerBackgroundColor(getResources().getColor(R.color.darkBlue));

        return view;
    }

    private void initView(View view) {
        mPieView = (PieView) view.findViewById(R.id.pieView);
        mHasilAkhir = (TextView) view.findViewById(R.id.hasil_akhir);
    }

    private void cekStatusSidang(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Mengecek status sidang...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangResponse> userCall = service.cekStatusSidang(user.getId_user());
        userCall.enqueue(new Callback<SidangResponse>() {
            @Override
            public void onResponse(Call<SidangResponse> call, Response<SidangResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus() && response.body().getData() != null){
                            getHasilSidang();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                }
            }
        });
    }

    private void getHasilSidang(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat data hasil sidang...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<HasilResponse> userCall = service.getHasilByMahasiswa(user.getId_user());

        userCall.enqueue(new Callback<HasilResponse>() {
            @Override
            public void onResponse(Call<HasilResponse> call, Response<HasilResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        Double nilaiPembimbing = Double.valueOf(response.body().getData().getRata2Pembimbing());
                        Double nilaiPenguji = Double.valueOf(response.body().getData().getRata2Penguji());
                        Double nilaiAkhir = nilaiPembimbing + nilaiPenguji;
                        mPieView.setPercentage(nilaiAkhir.intValue());
                        mPieView.setInnerText(String.format(Locale.US, "%.0f", nilaiAkhir));
                        PieAngleAnimation animation = new PieAngleAnimation(mPieView);
                        animation.setDuration(2000);
                        mPieView.startAnimation(animation);
                        mHasilAkhir.setText(response.body().getData().getHasil());
                    }
                }
            }

            @Override
            public void onFailure(Call<HasilResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                }
            }
        });
    }
}
