package polines.monitoringtugasakhir.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.UndanganSidangAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.Sidang;
import polines.monitoringtugasakhir.models.SidangListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UndanganSidangFragment extends Fragment {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private UndanganSidangAdapter mAdapter;
    private List<Sidang> modelList = new ArrayList<>();
    private UserTable user;

    public UndanganSidangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_undangan_sidang, container, false);

        user = UserTable.findById(UserTable.class, 1);
        findViews(view);

        getJadwalSidang();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getJadwalSidang();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });

        return view;
    }

    private void findViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
    }

    private void setAdapter() {


        mAdapter = new UndanganSidangAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new UndanganSidangAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position, final Sidang model) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_undangan_sidang);

                TextView mJudul = (TextView) dialog.findViewById(R.id.judul);
                TextView mTanggalSidang = (TextView) dialog.findViewById(R.id.tanggal_sidang);
                TextView mJamSidang = (TextView) dialog.findViewById(R.id.jam_sidang);
                TextView mRuangSidang = (TextView) dialog.findViewById(R.id.ruang_sidang);
                TextView mKetuaPenguji = (TextView) dialog.findViewById(R.id.ketua_penguji);
                TextView mSekretaris = (TextView) dialog.findViewById(R.id.sekretaris);
                TextView mPenguji1 = (TextView) dialog.findViewById(R.id.penguji_1);
                TextView mPenguji2 = (TextView) dialog.findViewById(R.id.penguji_2);
                TextView mPenguji3 = (TextView) dialog.findViewById(R.id.penguji_3);
                Button mBtnTutup = (Button) dialog.findViewById(R.id.btn_tutup);

                mJudul.setText(model.getTugasAkhir().getJudul());
                mTanggalSidang.setText(model.getTanggal());
                mJamSidang.setText(model.getJam());
                mRuangSidang.setText(model.getRuang());
                mKetuaPenguji.setText(model.getTugasAkhir().getPembimbing1().getNama());
                mSekretaris.setText(model.getSekretaris().getNama());
                mPenguji1.setText(model.getPenguji1().getNama());
                mPenguji2.setText(model.getPenguji2().getNama());
                mPenguji3.setText(model.getPenguji3().getNama());
                mBtnTutup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


    }

    private void getJadwalSidang() {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar jadwal sidang...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangListResponse> userCall = service.getPenilaian(user.getId_user());
        userCall.enqueue(new Callback<SidangListResponse>() {
            @Override
            public void onResponse(Call<SidangListResponse> call, Response<SidangListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            modelList = response.body().getData();
                            setAdapter();
                        } else {
                            Toasty.error(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toasty.error(getActivity(), getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    Toasty.error(getActivity(), getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
