package polines.monitoringtugasakhir.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.models.BimbinganTotalResponse;
import polines.monitoringtugasakhir.models.Sidang;
import polines.monitoringtugasakhir.models.SidangResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendaftaranSidangFragment extends Fragment {


    private TextView mJudul;
    private TextView mTanggalSidang;
    private TextView mJamSidang;
    private TextView mRuangSidang;
    private TextView mKetuaPenguji;
    private TextView mSekretaris;
    private TextView mPenguji1;
    private TextView mPenguji2;
    private TextView mPenguji3;
    private ActionProcessButton mBtnDaftar;
    private UserTable user;
    private CreateToast createToast;

    public PendaftaranSidangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pendaftaran_sidang, container, false);
        user = UserTable.findById(UserTable.class, 1);
        initView(view);
        createToast = new CreateToast(getActivity());

        cekStatusSidang();

        mBtnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekStatusBimbingan();
            }
        });

        return view;
    }

    private void initView(View view) {
        mJudul = (TextView) view.findViewById(R.id.judul);
        mTanggalSidang = (TextView) view.findViewById(R.id.tanggal_sidang);
        mJamSidang = (TextView) view.findViewById(R.id.jam_sidang);
        mRuangSidang = (TextView) view.findViewById(R.id.ruang_sidang);
        mKetuaPenguji = (TextView) view.findViewById(R.id.ketua_penguji);
        mSekretaris = (TextView) view.findViewById(R.id.sekretaris);
        mPenguji1 = (TextView) view.findViewById(R.id.penguji_1);
        mPenguji2 = (TextView) view.findViewById(R.id.penguji_2);
        mPenguji3 = (TextView) view.findViewById(R.id.penguji_3);
        mBtnDaftar = (ActionProcessButton) view.findViewById(R.id.btn_daftar);
    }

    private void cekStatusSidang(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat data sidang...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangResponse> userCall = service.cekStatusSidang(user.getId_user());
        userCall.enqueue(new Callback<SidangResponse>() {
            @Override
            public void onResponse(Call<SidangResponse> call, Response<SidangResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus() && response.body().getData() != null){
                            Sidang dataSidang = response.body().getData();
                            switch (dataSidang.getStatus()) {
                                case "1":
                                    mJudul.setText(dataSidang.getTugasAkhir().getJudul());
                                    mTanggalSidang.setText(dataSidang.getTanggal());
                                    mJamSidang.setText(dataSidang.getJam());
                                    mRuangSidang.setText(dataSidang.getRuang());
                                    mKetuaPenguji.setText(dataSidang.getTugasAkhir().getPembimbing1().getNama());
                                    mSekretaris.setText(dataSidang.getSekretaris().getNama());
                                    mPenguji1.setText(dataSidang.getPenguji1().getNama());
                                    mPenguji2.setText(dataSidang.getPenguji2().getNama());
                                    mPenguji3.setText(dataSidang.getPenguji3().getNama());
                                    mBtnDaftar.setProgress(100);
                                    mBtnDaftar.setText("Pendaftaran Selesai");
                                    mBtnDaftar.setEnabled(false);
                                    break;
                                case "0":
                                    mJudul.setText(dataSidang.getTugasAkhir().getJudul());
                                    mBtnDaftar.setProgress(100);
                                    mBtnDaftar.setText("Menunggu Konfirmasi");
                                    mBtnDaftar.setEnabled(false);
                                    break;
                                default:
                                    mJudul.setText(dataSidang.getTugasAkhir().getJudul());
                                    mBtnDaftar.setText("Daftar Sidang");
                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                }
            }
        });
    }

    private void showConfirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apakah anda yakin ingin mendaftar sidang ?");
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                storeSidang();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void cekStatusBimbingan(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Mengecek status bimbingan...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<BimbinganTotalResponse> userCall = service.getTotalBimbingan(user.getId_user());
        userCall.enqueue(new Callback<BimbinganTotalResponse>() {
            @Override
            public void onResponse(Call<BimbinganTotalResponse> call, Response<BimbinganTotalResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getData().getTotalBimbingan1() >= 8 && response.body().getData().getTotalBimbingan2() >= 8){
                            showConfirmDialog();
                        }else{
                            String message =
                                    "Anda harus bimbingan minimal 8 kali untuk mendaftar sidang.\n" +
                                            "\n" +
                                            "Total Bimbingan\n" +
                                            "Pembimbing 1: " + response.body().getData().getTotalBimbingan1() + " kali\n" +
                                            "Pembimbing 2: " + response.body().getData().getTotalBimbingan2() + " kali";
                            CreateDialog dialog = new CreateDialog();
                            dialog.showAlertDialog(getActivity(), "Pendaftaran Sidang Gagal", message);
                        }
                    }else{
                        createToast.internalServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<BimbinganTotalResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });
    }

    private void storeSidang(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Menyimpan data pendaftaran...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangResponse> userCall = service.storeSidang(String.valueOf(user.getId_user()));
        userCall.enqueue(new Callback<SidangResponse>() {
            @Override
            public void onResponse(Call<SidangResponse> call, Response<SidangResponse> response) {
                if (isAdded()){
                    if (response.isSuccessful()){
                        loading.closeProgressDialog();
                        if (response.body().getStatus()){
                            CreateDialog dialog = new CreateDialog();
                            dialog.showAlertDialog(getActivity(), "Pendaftaran Berhasil", "Mohon tunggu pendaftaran sidang anda dikonfirmasi oleh kaprodi");
                            cekStatusSidang();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    Toasty.error(getActivity(), getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
