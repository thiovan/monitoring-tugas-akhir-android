package polines.monitoringtugasakhir.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.BimbinganMahasiswaAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.UserInput;
import polines.monitoringtugasakhir.models.Bimbingan;
import polines.monitoringtugasakhir.models.BimbinganListResponse;
import polines.monitoringtugasakhir.models.BimbinganResponse;
import polines.monitoringtugasakhir.models.User;
import polines.monitoringtugasakhir.models.UserListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */


public class BimbinganTugasAkhirFragment extends Fragment {


    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private FloatingActionButton floatingActionButton;
    private Spinner spinner;
    private BimbinganMahasiswaAdapter mAdapter;
    private List<Bimbingan> modelList = new ArrayList<>();
    private List<Bimbingan> pembimbing1List = new ArrayList<>();
    private List<Bimbingan> pembimbing2List = new ArrayList<>();
    private UserTable user;
    private String selectedPembimbingId;
    private String[] items;

    public BimbinganTugasAkhirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bimbingan_tugas_akhir, container, false);
        user = UserTable.findById(UserTable.class, 1);
        findViews(view);

        getBimbingan();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_tambah_bimbingan);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                final MaterialEditText pembimbing = (MaterialEditText) dialog.findViewById(R.id.pembimbing);
                final MaterialEditText keterangan = (MaterialEditText) dialog.findViewById(R.id.keterangan);
                Button simpan = (Button) dialog.findViewById(R.id.btnSimpan);

                title.setText("Tambah Bimbingan");
                pembimbing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListPembimbing(pembimbing);
                    }
                });
                simpan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserInput userInput = new UserInput();
                        if (userInput.isValid(new MaterialEditText[]{pembimbing, keterangan})){
                            storeBimbingan(user.getId_user(), selectedPembimbingId, keterangan.getText().toString());
                            dialog.dismiss();
                        }
                    }
                });

                dialog.show();
            }
        });

        return view;
    }

    private void showListPembimbing(final MaterialEditText view){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar pembimbing...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserListResponse> userCall = service.getPembimbingListByMahasiswa(user.getId_user());
        userCall.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (isAdded()){
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            loading.closeProgressDialog();
                            List<User> listData = response.body().getData();
                            final String[] arrayData = new String[listData.size()];
                            final int[] arrayDataId = new int[listData.size()];

                            for (int i = 0; i < listData.size(); i++) {
                                arrayData[i] = listData.get(i).getNama();
                                arrayDataId[i] = listData.get(i).getId();
                            }
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                            dialog.setTitle("Pilih Pembimbing");
                            dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();
                                    selectedPembimbingId = String.valueOf(arrayDataId[i]);
                                    view.setText(arrayData[i]);
                                }

                            });

                            dialog.show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

               getBimbingan();
               swipeRefreshRecyclerList.setRefreshing(false);

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.darkBlue));
                if (items[i].equals(pembimbing1List.get(0).getPembimbing().getNama())){
                    setAdapter(pembimbing1List);
                }else if(items[i].equals(pembimbing2List.get(0).getPembimbing().getNama())){
                    setAdapter(pembimbing2List);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void findViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_tambah_bimbingan);
        spinner = view.findViewById(R.id.spinner);
    }

    private void setAdapter(List<Bimbingan> pembimbingList) {

        mAdapter = new BimbinganMahasiswaAdapter(getActivity(), pembimbingList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new BimbinganMahasiswaAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Bimbingan model) {
                if (model.getStatus().equals("0")){
                    CreateDialog dialog = new CreateDialog();
                    dialog.showAlertDialog(getActivity(), "Bimbingan Belum Dikonfirmasi", "Mohon minta konfirmasi ke pembimbing bahwa anda telah selesai melakukan bimbingan");
                }
            }
        });


    }

    private void getBimbingan(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", getString(R.string.mengambil_data_bimbingan));
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<BimbinganListResponse> userCall = service.bimbinganByMahasiswa(user.getId_user());

        userCall.enqueue(new Callback<BimbinganListResponse>() {
            @Override
            public void onResponse(Call<BimbinganListResponse> call, Response<BimbinganListResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            modelList = response.body().getData();
                            createSpinner();
                            createBimbinganList(modelList);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BimbinganListResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    Log.e("onFailure", t.getMessage());
                    Toasty.error(getActivity(), getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void storeBimbingan(Integer mahasiswa, String pembimbing, String keterangan){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", getString(R.string.sedang_menyimpan_data));
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<BimbinganResponse> userCall = service.storeBimbingan(String.valueOf(mahasiswa), pembimbing, keterangan);
        userCall.enqueue(new Callback<BimbinganResponse>() {
            @Override
            public void onResponse(Call<BimbinganResponse> call, Response<BimbinganResponse> response) {
                if (isAdded()){
                    if (response.isSuccessful()){
                        loading.closeProgressDialog();
                        if (response.body().getStatus()){
                            getBimbingan();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BimbinganResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    Toasty.error(getActivity(), getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createSpinner(){
        //membuat list pembimbing
        List<String> pembimbingList = new ArrayList<>();
        for (Bimbingan bimbingan : modelList) {
            pembimbingList.add(bimbingan.getPembimbing().getNama());
        }

        //membuat set pembimbing (remove duplicate value)
        Set<String> pembimbingSet = new HashSet<String>(pembimbingList);

        //convert set pembimbing ke array string
        items = pembimbingSet.toArray(new String[pembimbingSet.size()]);

        //Set spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
    }

    private void createBimbinganList(List<Bimbingan> modelList){
        pembimbing1List.clear();
        pembimbing2List.clear();

        for (int i=0; i<modelList.size(); i++) {
            if (modelList.get(i).getPembimbing().getId() == modelList.get(i).getTugasAkhir().getPembimbing1().getId()){
                pembimbing1List.add(modelList.get(i));
            }else if(modelList.get(i).getPembimbing().getId() == modelList.get(i).getTugasAkhir().getPembimbing2().getId()){
                pembimbing2List.add(modelList.get(i));
            }
        }
    }

}
