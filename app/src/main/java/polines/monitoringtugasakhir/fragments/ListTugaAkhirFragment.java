package polines.monitoringtugasakhir.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.ListTugasAkhirAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.TugasAkhir;
import polines.monitoringtugasakhir.models.TugasAkhirListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListTugaAkhirFragment extends Fragment {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private Spinner spinner;
    private ListTugasAkhirAdapter mAdapter;
    private List<TugasAkhir> modelList = new ArrayList<>();
    private List<TugasAkhir> tugasAkhirAktif = new ArrayList<>();
    private List<TugasAkhir> tugasAkhirSelesai = new ArrayList<>();
    private UserTable user;
    private String selectedPembimbingId;
    private String[] items;

    public ListTugaAkhirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_tuga_akhir, container, false);
        findViews(view);

        getTugasAkhir();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getTugasAkhir();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.darkBlue));
                if (i == 0){
                    setAdapter(tugasAkhirAktif);
                }else if(i == 1){
                    setAdapter(tugasAkhirSelesai);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        
        return view;
    }

    private void findViews(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
        spinner = view.findViewById(R.id.spinner);
    }

    private void setAdapter(List<TugasAkhir> pembimbingList) {

        mAdapter = new ListTugasAkhirAdapter(getActivity(), pembimbingList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new ListTugasAkhirAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, TugasAkhir model) {
                if (model.getStatus().equals("0")){
                    CreateDialog dialog = new CreateDialog();
                    dialog.showAlertDialog(getActivity(), "TugasAkhir Belum Dikonfirmasi", "Mohon minta konfirmasi ke pembimbing bahwa anda telah selesai melakukan bimbingan");
                }
            }
        });


    }

    private void getTugasAkhir(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar tugas akhir...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirListResponse> userCall = service.getAllTA();

        userCall.enqueue(new Callback<TugasAkhirListResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirListResponse> call, Response<TugasAkhirListResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        modelList = response.body().getData();
                        createSpinner();
                        createTugasAkhirList(modelList);
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirListResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                }
            }
        });
    }

    private void createSpinner(){
        items = new String[]{"Tugas Akhir Aktif", "Tugas Akhir Selesai"};

        //Set spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapter);
    }

    private void createTugasAkhirList(List<TugasAkhir> modelList){
        tugasAkhirAktif.clear();
        tugasAkhirSelesai.clear();

        for (int i=0; i<modelList.size(); i++) {
            if (modelList.get(i).getStatus().equals("1")){
                tugasAkhirAktif.add(modelList.get(i));
            }else if(modelList.get(i).getStatus().equals("2")){
                tugasAkhirSelesai.add(modelList.get(i));
            }
        }
    }

}
