package polines.monitoringtugasakhir.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.PenilaianPembimbingPengujiActivity;
import polines.monitoringtugasakhir.activities.RekapSekretarisActivity;
import polines.monitoringtugasakhir.adapters.TugasAkhirAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.models.Sidang;
import polines.monitoringtugasakhir.models.SidangListResponse;
import polines.monitoringtugasakhir.models.TugasAkhir;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenilaianFragment extends Fragment {


    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private TugasAkhirAdapter mAdapter;
    private List<Sidang> modelList = new ArrayList<>();
    private UserTable user;

    public PenilaianFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_penilaian, container, false);

        user = UserTable.findById(UserTable.class, 1);
        findViews(view);

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getPenilaianSidang();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getPenilaianSidang();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });


    }


    private void findViews(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
    }


    private void setAdapter() {


        mAdapter = new TugasAkhirAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new TugasAkhirAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position, final Sidang model) {
                final TextView sebagai = (TextView) view.findViewById(R.id.sebagai);

                TugasAkhir tugasAkhir = model.getTugasAkhir();
                final String[] arrayData;
                final int[] idUser;
                if (tugasAkhir.getPenyusun2() != null){
                    idUser = new int[]{
                            tugasAkhir.getPenyusun1().getId(),
                            tugasAkhir.getPenyusun2().getId()
                    };
                    arrayData = new String[]{
                            tugasAkhir.getPenyusun1().getNama(),
                            tugasAkhir.getPenyusun2().getNama()
                    };
                } else if (tugasAkhir.getPenyusun3() != null){
                    idUser = new int[]{
                            tugasAkhir.getPenyusun1().getId(),
                            tugasAkhir.getPenyusun2().getId(),
                            tugasAkhir.getPenyusun3().getId()
                    };
                    arrayData = new String[]{
                            tugasAkhir.getPenyusun1().getNama(),
                            tugasAkhir.getPenyusun2().getNama(),
                            tugasAkhir.getPenyusun3().getNama()
                    };
                } else {
                    idUser = new int[]{tugasAkhir.getPenyusun1().getId()};
                    arrayData = new String[]{tugasAkhir.getPenyusun1().getNama()};
                }

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("Pilih Mahasiswa");
                dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                        Intent intent;
                        if (sebagai.getText().toString().equals("Sekretaris")){
                            intent = new Intent(getActivity(), RekapSekretarisActivity.class);
                        }else{
                            intent = new Intent(getActivity(), PenilaianPembimbingPengujiActivity.class);
                        }

                        intent.putExtra("ID_MAHASISWA", idUser[i]);
                        intent.putExtra("NAMA_MAHASISWA", arrayData[i]);
                        intent.putExtra("SEBAGAI", sebagai.getText().toString());
                        intent.putExtra("ID_TUGAS_AKHIR", model.getTugasAkhir().getId());
                        startActivity(intent);
                    }

                });
                dialog.show();
            }
        });


    }

    private void getPenilaianSidang() {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar penilaian sidang...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangListResponse> userCall = service.getPenilaian(user.getId_user());
        userCall.enqueue(new Callback<SidangListResponse>() {
            @Override
            public void onResponse(Call<SidangListResponse> call, Response<SidangListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            modelList = response.body().getData();
                            setAdapter();
                        } else {
                            Toasty.error(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toasty.error(getActivity(), getString(R.string.internal_server_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    Toasty.error(getActivity(), getString(R.string.connection_failure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getPenilaianSidang();
    }
}
