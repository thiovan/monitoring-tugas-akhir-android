package polines.monitoringtugasakhir.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;

import io.feeeei.circleseekbar.CircleSeekBar;
import polines.monitoringtugasakhir.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenilaianPembimbingFragment extends Fragment {


    private TextView mNilai1;
    private TextView mTotalNilai1;
    private ActionProcessButton mBtnNilai1;

    public PenilaianPembimbingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_penilaian_pembimbing, container, false);
        initView(view);

        mBtnNilai1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_input_nilai);

                final CircleSeekBar seekBar = (CircleSeekBar) dialog.findViewById(R.id.seekbar);
                final TextView nilai = (TextView) dialog.findViewById(R.id.nilai);

                seekBar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onChanged(CircleSeekBar circleSeekBar, int i) {
                        nilai.setText(String.valueOf(i));
                    }
                });

                dialog.show();
            }
        });

        return view;
    }

    private void initView(View view) {
        mNilai1 = (TextView) view.findViewById(R.id.nilai_1);
        mTotalNilai1 = (TextView) view.findViewById(R.id.total_nilai_1);
        mBtnNilai1 = (ActionProcessButton) view.findViewById(R.id.btn_nilai_1);
    }
}
