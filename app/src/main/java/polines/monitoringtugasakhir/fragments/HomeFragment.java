package polines.monitoringtugasakhir.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.HomeActivity;
import polines.monitoringtugasakhir.entities.UserTable;


public class HomeFragment extends Fragment {


    private TextView mTvTipe;
    private TextView mTvNama;
    private TextView mTvNimorInduk;
    private RelativeLayout mMenu1;
    private RelativeLayout mMenu2;
    private RelativeLayout mMenu3;
    private RelativeLayout mMenu4;
    private RelativeLayout mMenu5;
    private UserTable user;
    private View view;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = UserTable.findById(UserTable.class, 1);
        if (user.getTipe().equals("Mahasiswa")){
            view = inflater.inflate(R.layout.fragment_home_mahasiswa, container, false);
            initView(view);
            menuMahasiswa();
        }else if (user.getTipe().equals("Dosen")){
            view = inflater.inflate(R.layout.fragment_home_dosen, container, false);
            initView(view);
            menuDosen();
        }else if (user.getTipe().equals("Admin")){
            view = inflater.inflate(R.layout.fragment_home_admin, container, false);
            initView(view);
            menuAdmin();
        }

        mTvNama.setText(user.getNama());
        mTvNimorInduk.setText(user.getNomorInduk());

        return view;
    }

    private void initView(View view) {
        mTvTipe = (TextView) view.findViewById(R.id.tvTipe);
        mTvNama = (TextView) view.findViewById(R.id.tvNama);
        mTvNimorInduk = (TextView) view.findViewById(R.id.tv_nimor_induk);
    }

    private void menuMahasiswa(){
        mMenu1 = (RelativeLayout) view.findViewById(R.id.menu_1);
        mMenu2 = (RelativeLayout) view.findViewById(R.id.menu_2);
        mMenu3 = (RelativeLayout) view.findViewById(R.id.menu_3);
        mMenu4 = (RelativeLayout) view.findViewById(R.id.menu_4);

        mMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 2);
            }
        });

        mMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 3);
            }
        });

        mMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 4);
            }
        });

        mMenu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 5);
            }
        });
    }

    private void menuDosen(){
        mMenu1 = (RelativeLayout) view.findViewById(R.id.menu_1);
        mMenu2 = (RelativeLayout) view.findViewById(R.id.menu_2);
        mMenu3 = (RelativeLayout) view.findViewById(R.id.menu_3);

        mMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 2);
            }
        });

        mMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 3);
            }
        });

        mMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 4);
            }
        });
    }

    private void menuAdmin(){
        mMenu1 = (RelativeLayout) view.findViewById(R.id.menu_1);
        mMenu2 = (RelativeLayout) view.findViewById(R.id.menu_2);
        mMenu3 = (RelativeLayout) view.findViewById(R.id.menu_3);
        mMenu4 = (RelativeLayout) view.findViewById(R.id.menu_4);
        mMenu5 = (RelativeLayout) view.findViewById(R.id.menu_5);

        mMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 2);
            }
        });

        mMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 3);
            }
        });

        mMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 4);
            }
        });

        mMenu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 5);
            }
        });

        mMenu5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).setSelectedFragment(user.getTipe(), 6);
            }
        });
    }
}
