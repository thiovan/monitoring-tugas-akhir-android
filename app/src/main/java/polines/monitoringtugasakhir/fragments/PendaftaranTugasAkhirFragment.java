package polines.monitoringtugasakhir.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.github.thunder413.datetimeutils.DateTimeUnits;
import com.github.thunder413.datetimeutils.DateTimeUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import cn.iwgang.countdownview.CountdownView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.HomeActivity;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.helper.UserInput;
import polines.monitoringtugasakhir.models.TugasAkhir;
import polines.monitoringtugasakhir.models.TugasAkhirResponse;
import polines.monitoringtugasakhir.models.User;
import polines.monitoringtugasakhir.models.UserListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendaftaranTugasAkhirFragment extends Fragment {

    private View view;
    private MaterialEditText mJudul;
    private MaterialEditText mPembimbing1;
    private MaterialEditText mPembimbing2;
    private MaterialEditText mPenyusun1;
    private MaterialEditText mPenyusun2;
    private MaterialEditText mPenyusun3;
    private Button mBtnTambahPenyusun;
    private Button mBtnHspusPenyusun;
    private Button mBtnUploadProposal;
    private ActionProcessButton mBtnDaftar;
    private CountdownView mCountdown;
    private CardView mCountdownContainer;
    private UserTable user;
    private int jumlahPenyusun = 1;
    private String[] data = {"", "", "", "", "", "", ""};
    private UserInput userInput;
    private View[] views;
    private CreateToast createToast;
    private MultipartBody.Part proposalFile = null;
    private RequestBody proposalFileName = null;

    public PendaftaranTugasAkhirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pendaftaran_tugas_akhir, container, false);
        initView(view);

        views = new View[]{mJudul, mPembimbing1, mPembimbing2, mPenyusun1, mPenyusun2, mPenyusun3, mBtnTambahPenyusun, mBtnHspusPenyusun, mBtnUploadProposal, mBtnDaftar};

        user = UserTable.findById(UserTable.class, 1);

        userInput = new UserInput();

        createToast = new CreateToast(getActivity());

        cekStatusTA();

        mPenyusun1.setText(user.getNama());

        mPembimbing1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListPembimbingDialog(getString(R.string.pembimbing), mPembimbing1, 4);
            }
        });

        mPembimbing2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListPembimbingDialog(getString(R.string.pembimbing), mPembimbing2, 5);
            }
        });

        mBtnTambahPenyusun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumlahPenyusun++;
                addRemovePenyusun();
            }
        });

        mBtnHspusPenyusun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jumlahPenyusun--;
                addRemovePenyusun();
            }
        });

        mBtnUploadProposal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dexter.withActivity(getActivity())
                        .withPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                new MaterialFilePicker()
                                        .withActivity(getActivity())
                                        .withRequestCode(1)
                                        .withHiddenFiles(false)
                                        .start();
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
                        }).check();
            }
        });

        mPenyusun2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListMahasiswaDialog(getString(R.string.mahasiswa), mPenyusun2, 2);
            }
        });

        mPenyusun3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showListMahasiswaDialog(getString(R.string.mahasiswa), mPenyusun3, 3);
            }
        });

        mBtnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputValid()){
                    userInput.disable(views);
                    mBtnDaftar.setProgress(1);
                    data[0] = mJudul.getText().toString();
                    data[1] = String.valueOf(user.getId_user());
                    data[6] = "0";
                    sendDataToServer(data);
                }
            }
        });

        return view;
    }

    private void initView(View view) {
        mJudul = (MaterialEditText) view.findViewById(R.id.judul);
        mPembimbing1 = (MaterialEditText) view.findViewById(R.id.pembimbing_1);
        mPembimbing2 = (MaterialEditText) view.findViewById(R.id.pembimbing_2);
        mPenyusun1 = (MaterialEditText) view.findViewById(R.id.penyusun_1);
        mPenyusun2 = (MaterialEditText) view.findViewById(R.id.penyusun_2);
        mPenyusun3 = (MaterialEditText) view.findViewById(R.id.penyusun_3);
        mBtnTambahPenyusun = (Button) view.findViewById(R.id.btn_tambah_penyusun);
        mBtnHspusPenyusun = (Button) view.findViewById(R.id.btn_hapus_penyusun);
        mBtnUploadProposal = (Button) view.findViewById(R.id.btn_upload_proposal);
        mBtnDaftar = (ActionProcessButton) view.findViewById(R.id.btn_daftar);
        mCountdown = (CountdownView) view.findViewById(R.id.countdown);
        mCountdownContainer = (CardView) view.findViewById(R.id.countdown_container);
    }

    private void cekStatusTA(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat data tugas akhir...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirResponse> userCall = service.cekStatusTA(user.getId_user());
        userCall.enqueue(new Callback<TugasAkhirResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirResponse> call, Response<TugasAkhirResponse> response) {
                loading.closeProgressDialog();
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getData() != null) {
                            TugasAkhir tugasAkhir = response.body().getData();
                            if (response.body().getData().getStatus().equals("0")) {
                                userInput.disable(views);
                                mBtnDaftar.setProgress(100);
                                mBtnDaftar.setText("Menunggu Konfirmasi");
                                mJudul.setText(response.body().getData().getJudul());
                                mPembimbing1.setText(tugasAkhir.getPembimbing1().getNama());
                                mPembimbing2.setText(tugasAkhir.getPembimbing2().getNama());
                                mPenyusun1.setText(tugasAkhir.getPenyusun1().getNama());
                                if (response.body().getData().getPenyusun2() != null) {
                                    mPenyusun2.setVisibility(View.VISIBLE);
                                    mPenyusun2.setText(tugasAkhir.getPenyusun2().getNama());
                                }
                                if (response.body().getData().getPenyusun3() != null) {
                                    mPenyusun3.setVisibility(View.VISIBLE);
                                    mPenyusun3.setText(tugasAkhir.getPenyusun3().getNama());
                                }
                            } else if (response.body().getData().getStatus().equals("1") || response.body().getData().getStatus().equals("2")) {
                                userInput.disable(views);
                                mBtnDaftar.setProgress(100);
                                mBtnDaftar.setText("Pendaftaran Selesai");
                                mJudul.setText(response.body().getData().getJudul());
                                mPembimbing1.setText(tugasAkhir.getPembimbing1().getNama());
                                mPembimbing2.setText(tugasAkhir.getPembimbing2().getNama());
                                mPenyusun1.setText(tugasAkhir.getPenyusun1().getNama());
                                if (response.body().getData().getPenyusun2() != null) {
                                    mPenyusun2.setVisibility(View.VISIBLE);
                                    mPenyusun2.setText(tugasAkhir.getPenyusun2().getNama());
                                }
                                if (response.body().getData().getPenyusun3() != null) {
                                    mPenyusun3.setVisibility(View.VISIBLE);
                                    mPenyusun3.setText(tugasAkhir.getPenyusun3().getNama());
                                }
                                mCountdownContainer.setVisibility(View.VISIBLE);
                                startCountdown(response.body().getData().getCreated_at());
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });
    }

    private void showListPembimbingDialog(final String title, final MaterialEditText view, final int dataIndex) {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar "+ title +"...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserListResponse> userCall = service.getPembimbingList();
        userCall.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        List<User> listData = response.body().getData();
                        final String[] arrayData = new String[listData.size()];
                        final int[] arrayDataId = new int[listData.size()];

                        for (int i = 0; i < listData.size(); i++) {
                            arrayData[i] = listData.get(i).getNama();
                            arrayDataId[i] = listData.get(i).getId();
                        }

                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setTitle("Pilih " + title);
                        dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                                data[dataIndex] = String.valueOf(arrayDataId[i]);
                                view.setText(arrayData[i]);
                            }

                        });

                        dialog.show();
                    }
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });

    }

    private void showListMahasiswaDialog(final String title, final MaterialEditText view, final int dataIndex) {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar "+ title +"...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserListResponse> userCall = service.getMahasiswaList();
        userCall.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        List<User> listData = response.body().getData();
                        final String[] arrayData = new String[listData.size()];
                        final int[] arrayDataId = new int[listData.size()];

                        for (int i = 0; i < listData.size(); i++) {
                            arrayData[i] = listData.get(i).getNama();
                            arrayDataId[i] = listData.get(i).getId();
                        }

                        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                        dialog.setTitle("Pilih " + title);
                        dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                                data[dataIndex] = String.valueOf(arrayDataId[i]);
                                view.setText(arrayData[i]);
                            }

                        });

                        dialog.show();
                    }
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });

    }

    private void addRemovePenyusun(){
        if (jumlahPenyusun == 1) {
            mPenyusun2.setVisibility(View.GONE);
            mPenyusun2.setText("");
            mPenyusun3.setVisibility(View.GONE);
            mPenyusun3.setText("");
            mBtnTambahPenyusun.setEnabled(true);
            mBtnHspusPenyusun.setEnabled(false);
        }else if(jumlahPenyusun == 2){
            mPenyusun2.setVisibility(View.VISIBLE);
            mPenyusun3.setVisibility(View.GONE);
            mPenyusun3.setText("");
            mBtnTambahPenyusun.setEnabled(true);
            mBtnHspusPenyusun.setEnabled(true);
        }else if (jumlahPenyusun == 3){
            mPenyusun2.setVisibility(View.VISIBLE);
            mPenyusun3.setVisibility(View.VISIBLE);
            mBtnTambahPenyusun.setEnabled(false);
            mBtnHspusPenyusun.setEnabled(true);
        }
    }

    private boolean inputValid(){
        MaterialEditText[] inputView;
        if (jumlahPenyusun == 2){
            inputView = new MaterialEditText[]{mJudul, mPembimbing1, mPembimbing2, mPenyusun1, mPenyusun2};
        }else if (jumlahPenyusun == 3){
            inputView = new MaterialEditText[]{mJudul, mPembimbing1, mPembimbing2, mPenyusun1, mPenyusun2, mPenyusun3};
        }else{
            inputView = new MaterialEditText[]{mJudul, mPembimbing1, mPembimbing2, mPenyusun1};
        }

        for (int i=0; i < inputView.length; i++){
            if (inputView[i].getText().toString().matches("")){
                inputView[i].setError(getString(R.string.empty_field));
                return false;
            }
        }

        if (proposalFile == null && proposalFileName == null){
            return  false;
        }

        return true;
    }

    private void sendDataToServer(String[] datas) {
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirResponse> userCall = service.storeTugasAkhir(
                RequestBody.create(MediaType.parse("text/plain"), data[0]),
                RequestBody.create(MediaType.parse("text/plain"), data[1]),
                RequestBody.create(MediaType.parse("text/plain"), data[2]),
                RequestBody.create(MediaType.parse("text/plain"), data[3]),
                RequestBody.create(MediaType.parse("text/plain"), data[4]),
                RequestBody.create(MediaType.parse("text/plain"), data[5]),
                RequestBody.create(MediaType.parse("text/plain"), data[6]),
                proposalFile,
                proposalFileName);
        userCall.enqueue(new Callback<TugasAkhirResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirResponse> call, Response<TugasAkhirResponse> response) {
                if (isAdded()){
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            userInput.disable(views);
                            mBtnDaftar.setProgress(100);
                            mBtnDaftar.setText("Menunggu Konfirmasi");
                            CreateDialog dialog = new CreateDialog();
                            dialog.showAlertDialog(getActivity(), "Pendaftaran Berhasil", "Mohon tunggu pendaftaran tugas akhir anda dikonfirmasi oleh kaprodi");
                        }else{
                            userInput.enable(views);
                            mBtnDaftar.setProgress(0);
                            createToast.error(response.body().getMessage());
                        }
                    }else{
                        userInput.enable(views);
                        mBtnDaftar.setProgress(0);
                        createToast.internalServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirResponse> call, Throwable t) {
                if (isAdded()){
                    mBtnDaftar.setProgress(0);
                    createToast.onFailure();
                }
            }
        });
    }

    private void startCountdown(String created_at) {
        Date date = DateTimeUtils.formatDate(created_at);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 90);

        Date dateNow = new Date();
        long diff = cal.getTime().getTime() - dateNow.getTime();
        mCountdown.start(diff);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1 && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            Log.e("file", filePath);
            File file = new File(filePath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            //RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("proposal", file.getName(), mFile);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

            proposalFile = fileToUpload;
            proposalFileName = filename;
        }
    }

}
