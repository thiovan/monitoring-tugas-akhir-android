package polines.monitoringtugasakhir.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v14.preference.SwitchPreference;
import android.support.v4.app.Fragment;
import android.support.v7.preference.Preference;
import android.widget.Toast;

import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.AuthorActivity;
import polines.monitoringtugasakhir.config.Config;

/**
 * A simple {@link Fragment} subclass.
 */
public class PengaturanFragment extends PreferenceFragmentCompat {

    public PengaturanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreatePreferencesFix(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.fragment_pengaturan, rootKey);

        final SwitchPreference notifikasi = (SwitchPreference) findPreference("notifikasi");

        notifikasi.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean switched = ((SwitchPreference) preference).isChecked();
                if (switched){
                    Toasty.error(getActivity(), "Notifikasi Pemberitahuan Dimatikan", Toast.LENGTH_SHORT).show();
                    notifikasi.setChecked(false);
                }else{
                    Toasty.success(getActivity(), "Notifikasi Pemberitahuan Dinyalakan", Toast.LENGTH_SHORT).show();
                    notifikasi.setChecked(true);
                }
                return false;
            }
        });

        Preference author = (Preference) findPreference("author");
        author.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(getActivity(), AuthorActivity.class);
                startActivity(intent);
                return false;
            }
        });

        Preference version = (Preference) findPreference("version");
        version.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Config.APP_UPDATE_URL));
                startActivity(intent);
                return false;
            }
        });
    }

}


