package polines.monitoringtugasakhir.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.dd.processbutton.iml.ActionProcessButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.KonfirmasiTugasAkhirAdapter;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.models.TugasAkhir;
import polines.monitoringtugasakhir.models.TugasAkhirListResponse;
import polines.monitoringtugasakhir.models.TugasAkhirResponse;
import polines.monitoringtugasakhir.models.User;
import polines.monitoringtugasakhir.models.UserListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */


public class KonfirmasiTugasAkhirFragment extends Fragment {


    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private KonfirmasiTugasAkhirAdapter mAdapter;

    private List<TugasAkhir> modelList = new ArrayList<>();
    private String[] selectedPembimbing = {"", ""};
    private CreateToast createToast;


    public KonfirmasiTugasAkhirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_konfirmasi_tugas_akhir, container, false);

        findViews(view);
        createToast = new CreateToast(getActivity());

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDaftarKonfirmasiTA();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getDaftarKonfirmasiTA();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });


    }


    private void findViews(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
    }

    private void setAdapter() {


        mAdapter = new KonfirmasiTugasAkhirAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new KonfirmasiTugasAkhirAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, final TugasAkhir model) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_konfimasi_tugas_akhir);

                final MaterialEditText pembimbing1 = (MaterialEditText) dialog.findViewById(R.id.pembimbing_1);
                selectedPembimbing[0] = String.valueOf(modelList.get(position).getPembimbing1().getId());
                pembimbing1.setText(modelList.get(position).getPembimbing1().getNama());
                pembimbing1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog(getString(R.string.pembimbing), pembimbing1, 0);
                    }
                });
                final MaterialEditText pembimbing2 = (MaterialEditText) dialog.findViewById(R.id.pembimbing_2);
                selectedPembimbing[1] = String.valueOf(modelList.get(position).getPembimbing2().getId());
                pembimbing2.setText(modelList.get(position).getPembimbing2().getNama());
                pembimbing2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog(getString(R.string.pembimbing), pembimbing2, 1);
                    }
                });
                ActionProcessButton btnKonfirmasi = (ActionProcessButton) dialog.findViewById(R.id.btn_konfirmasi);
                btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        updateTA(model.getId(), selectedPembimbing[0], selectedPembimbing[1], "1");
                    }
                });

                dialog.show();
            }
        });


    }

    private void showListDialog(final String title, final MaterialEditText view, final int index) {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar "+ title +"...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserListResponse> userCall = service.getPembimbingList();
        userCall.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    List<User> listData = response.body().getData();
                    final String[] arrayData = new String[listData.size()];
                    final int[] arrayDataId = new int[listData.size()];

                    for (int i = 0; i < listData.size(); i++) {
                        arrayData[i] = listData.get(i).getNama();
                        arrayDataId[i] = listData.get(i).getId();
                    }

                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle("Pilih " + title);
                    dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                            selectedPembimbing[index] = String.valueOf(arrayDataId[i]);
                            view.setText(arrayData[i]);
                        }

                    });

                    dialog.show();
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });

    }

    private void getDaftarKonfirmasiTA(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar konfirmasi tugas akhir...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirListResponse> userCall = service.getDaftarKonfirmasiTA();
        userCall.enqueue(new Callback<TugasAkhirListResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirListResponse> call, Response<TugasAkhirListResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            modelList = response.body().getData();
                            setAdapter();
                        }
                    }else{
                        createToast.internalServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirListResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });
    }

    private void updateTA(int id, String pembimbing_1, String pembimbing_2, String status){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Menyimpan data konfirmasi...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirResponse> userCall = service.updateTugasAkhir(id, pembimbing_1, pembimbing_2, status);
        userCall.enqueue(new Callback<TugasAkhirResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirResponse> call, Response<TugasAkhirResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            createToast.success("Konfirmasi Berhasil");
                            getDaftarKonfirmasiTA();
                        }
                    }else{
                        createToast.internalServerError();
                        getDaftarKonfirmasiTA();
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    createToast.onFailure();
                    getDaftarKonfirmasiTA();
                }
            }
        });
    }

}
