package polines.monitoringtugasakhir.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.KonfirmasiBimbinganActivity;
import polines.monitoringtugasakhir.adapters.KonfirmasiBimbinganAdapter;
import polines.monitoringtugasakhir.entities.UserTable;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.models.TugasAkhir;
import polines.monitoringtugasakhir.models.TugasAkhirListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class KonfirmasiBimbinganFragment extends Fragment {


    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private KonfirmasiBimbinganAdapter mAdapter;
    private List<TugasAkhir> modelList = new ArrayList<>();
    private UserTable user;
    private CreateToast createToast;

    public KonfirmasiBimbinganFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_konfirmasi_bimbingan, container, false);

        user = UserTable.findById(UserTable.class, 1);
        findViews(view);
        createToast = new CreateToast(getActivity());

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getBimbingan();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getBimbingan();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });


    }


    private void findViews(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
    }


    private void setAdapter() {


        mAdapter = new KonfirmasiBimbinganAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new KonfirmasiBimbinganAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position, final TugasAkhir model) {
                Intent intent = new Intent(getActivity(), KonfirmasiBimbinganActivity.class);
                intent.putExtra("ID_TA", model.getId());
                startActivity(intent);
            }
        });


    }
    
    private void getBimbingan(){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar bimbingan...");
        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<TugasAkhirListResponse> userCall = service.getBimbinganTugasAkhir(user.getId_user());
        userCall.enqueue(new Callback<TugasAkhirListResponse>() {
            @Override
            public void onResponse(Call<TugasAkhirListResponse> call, Response<TugasAkhirListResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getData().size() > 0){
                            modelList = response.body().getData();
                            setAdapter();
                        }else{
                            createToast.warning("Daftar Bimbingan Kosong");
                        }
                    }else{
                        createToast.internalServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<TugasAkhirListResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    createToast.onFailure();
                }
            }
        });
    }

}
