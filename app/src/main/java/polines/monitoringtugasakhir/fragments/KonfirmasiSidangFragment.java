package polines.monitoringtugasakhir.fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.adapters.KonfirmasiSidangAdapter;
import polines.monitoringtugasakhir.helper.CreateDialog;
import polines.monitoringtugasakhir.helper.CreateToast;
import polines.monitoringtugasakhir.helper.UserInput;
import polines.monitoringtugasakhir.models.Sidang;
import polines.monitoringtugasakhir.models.SidangListResponse;
import polines.monitoringtugasakhir.models.SidangResponse;
import polines.monitoringtugasakhir.models.User;
import polines.monitoringtugasakhir.models.UserListResponse;
import polines.monitoringtugasakhir.network.APIClient;
import polines.monitoringtugasakhir.network.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */


public class KonfirmasiSidangFragment extends Fragment {


    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshRecyclerList;
    private KonfirmasiSidangAdapter mAdapter;
    private List<Sidang> modelList = new ArrayList<>();
    private String[] data = {"", "", "", ""};
    private CreateToast toast;

    public KonfirmasiSidangFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_konfirmasi_tugas_akhir, container, false);

        findViews(view);
        toast = new CreateToast(getActivity());

        return view;

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getKonfirmasiSidang();

        swipeRefreshRecyclerList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getKonfirmasiSidang();
                swipeRefreshRecyclerList.setRefreshing(false);

            }
        });


    }


    private void findViews(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        swipeRefreshRecyclerList = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_recycler_list);
    }


    private void setAdapter() {


        mAdapter = new KonfirmasiSidangAdapter(getActivity(), modelList);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


        recyclerView.setAdapter(mAdapter);


        mAdapter.SetOnItemClickListener(new KonfirmasiSidangAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position, final Sidang model) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_konfirmasi_sidang);

                final MaterialEditText mTanggal = (MaterialEditText) dialog.findViewById(R.id.tanggal);
                final MaterialEditText mJam = (MaterialEditText) dialog.findViewById(R.id.jam);
                final MaterialEditText  mRuang = (MaterialEditText) dialog.findViewById(R.id.ruang);
                final MaterialEditText mSekretaris = (MaterialEditText) dialog.findViewById(R.id.sekretaris);
                final MaterialEditText mPenguji1 = (MaterialEditText) dialog.findViewById(R.id.penguji_1);
                final MaterialEditText mPenguji2 = (MaterialEditText) dialog.findViewById(R.id.penguji_2);
                final MaterialEditText mPenguji3 = (MaterialEditText) dialog.findViewById(R.id.penguji_3);
                ActionProcessButton mBtnDaftar = (ActionProcessButton) dialog.findViewById(R.id.btn_daftar);

                final Calendar myCalendar = Calendar.getInstance();
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "dd/MM/yyyy";
                        SimpleDateFormat formatedDate = new SimpleDateFormat(myFormat, Locale.US);

                        mTanggal.setText(formatedDate.format(myCalendar.getTime()));
                    }

                };
                final TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mJam.setText( String.format(Locale.US, "%02d", selectedHour) + ":" + String.format("%02d", selectedMinute));
                    }
                }, hour, minute, true);

                mTanggal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new DatePickerDialog(getActivity(), date, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });

                mJam.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mTimePicker.show();
                    }
                });

                mSekretaris.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog("Sekretaris", mSekretaris, 0);
                    }
                });

                mPenguji1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog("Penguji", mPenguji1, 1);
                    }
                });

                mPenguji2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog("Penguji", mPenguji2, 2);
                    }
                });

                mPenguji3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showListDialog("Penguji", mPenguji3, 3);
                    }
                });

                mBtnDaftar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserInput userInput = new UserInput();
                        if(userInput.isValid(new MaterialEditText[]{mTanggal, mJam, mRuang, mSekretaris, mPenguji1, mPenguji2, mPenguji3})){
                            updateSidang(
                                    model.getId(),
                                    mTanggal.getText().toString(),
                                    mJam.getText().toString(),
                                    mRuang.getText().toString(),
                                    data[0],
                                    data[1],
                                    data[2],
                                    data[3],
                                    "1"
                            );
                            dialog.dismiss();
                        }else{
                            Toasty.error(getActivity(), "Ada Field Yang Masih Kosong", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                dialog.show();
            }
        });


    }

    private void showListDialog(final String title, final MaterialEditText view, final int index) {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar " + title + "...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<UserListResponse> userCall = service.getPembimbingList();
        userCall.enqueue(new Callback<UserListResponse>() {
            @Override
            public void onResponse(Call<UserListResponse> call, Response<UserListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    List<User> listData = response.body().getData();
                    final String[] arrayData = new String[listData.size()];
                    final int[] arrayDataId = new int[listData.size()];

                    for (int i = 0; i < listData.size(); i++) {
                        arrayData[i] = listData.get(i).getNama();
                        arrayDataId[i] = listData.get(i).getId();
                    }

                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle("Pilih " + title);
                    dialog.setItems(arrayData, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                            data[index] = String.valueOf(arrayDataId[i]);
                            view.setText(arrayData[i]);
                        }

                    });

                    dialog.show();
                }

            }

            @Override
            public void onFailure(Call<UserListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    Toast.makeText(getActivity(), "Gagal memuat data", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getKonfirmasiSidang() {
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Memuat daftar konfirmasi sidang...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangListResponse> userCall = service.getKonfirmasiSidang();
        userCall.enqueue(new Callback<SidangListResponse>() {
            @Override
            public void onResponse(Call<SidangListResponse> call, Response<SidangListResponse> response) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            modelList = response.body().getData();
                            setAdapter();
                        } else {
                            toast.error(response.body().getMessage());
                        }
                    } else {
                        toast.internalServerError();
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangListResponse> call, Throwable t) {
                if (isAdded()) {
                    loading.closeProgressDialog();
                    toast.onFailure();
                }
            }
        });
    }

    private void updateSidang(Integer id, String tanggal, String jam, String ruang, String sekretaris, String penguji_1, String penguji_2, String penguji_3, String status){
        final CreateDialog loading = new CreateDialog();
        loading.showProgressDialog(getActivity(), "", "Menyimpan data sidang...");

        APIInterface service = APIClient.getClient().create(APIInterface.class);
        Call<SidangResponse> userCall = service.updateSidang(id, tanggal, jam, ruang, sekretaris, penguji_1, penguji_2, penguji_3, status);
        userCall.enqueue(new Callback<SidangResponse>() {
            @Override
            public void onResponse(Call<SidangResponse> call, Response<SidangResponse> response) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus()){
                            toast.success("Konfirmasi Berhasil");
                            getKonfirmasiSidang();
                        }else{
                            toast.error(response.body().getMessage());
                            getKonfirmasiSidang();
                        }
                    }else{
                        toast.internalServerError();
                        getKonfirmasiSidang();
                    }
                }
            }

            @Override
            public void onFailure(Call<SidangResponse> call, Throwable t) {
                if (isAdded()){
                    loading.closeProgressDialog();
                    toast.onFailure();
                    getKonfirmasiSidang();
                }
            }
        });
    }

}
