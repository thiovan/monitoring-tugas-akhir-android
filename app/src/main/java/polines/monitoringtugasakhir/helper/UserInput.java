package polines.monitoringtugasakhir.helper;

import android.view.View;

import com.rengwuxian.materialedittext.MaterialEditText;

public class UserInput {

    public void enable(View[] views){
        for (View view : views) {
            view.setEnabled(true);
        }
    }

    public void disable(View[] views){
        for (View view : views) {
            view.setEnabled(false);
        }
    }

    public boolean isValid(MaterialEditText[] editTexts){
        for (MaterialEditText editText : editTexts) {
            if (editText.getText().toString().matches("")){
                return false;
            }
        }

        return true;
    }

}
