package polines.monitoringtugasakhir.helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.Spanned;

import java.util.Map;

import polines.monitoringtugasakhir.R;
import polines.monitoringtugasakhir.activities.HomeActivity;
import polines.monitoringtugasakhir.entities.NotificationTable;

public class NotificationUtils {
    private static String TAG = NotificationUtils.class.getSimpleName();

    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public void show(Map<String, String> data, NotificationManager notificationManager) {
        Long timestamp = System.currentTimeMillis() / 1000;
        int notificationId = timestamp.intValue();

        Intent notificationIntent = new Intent(mContext, HomeActivity.class);
//        if (data.get("title").substring(0, 10).equals("Pengumuman")) {
//            notificationIntent.putExtra("ID", notificationId);
//            notificationIntent.setAction("pengumuman");
//        } else if (data.get("title").substring(0, 11).equals("Surat Masuk")){
//            notificationIntent.putExtra("ID", notificationId);
//            notificationIntent.setAction("surat_masuk");
//        } else if (data.get("title").substring(0, 11).equals("Surat Tidak")) {
//            notificationIntent.putExtra("ID", notificationId);
//            notificationIntent.setAction("surat_tidak_mengajar");
//        } else {
//            notificationIntent.putExtra("ID", notificationId);
//            notificationIntent.setAction("home");
//        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                // these are the three things a NotificationCompat.Builder object requires at a minimum
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.drawable.ic_graduation_cap)
                .setContentTitle(data.get("title"))
                .setContentText(fromHtml("<b>" + data.get("inner_title") + "</b><br><br>" + data.get("message")))
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getService(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(0)
                .setLights(0xFFFFFFFF, 1000, 2000)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(fromHtml("<b>" + data.get("inner_title") + "</b><br><br>" + data.get("message"))));

        notificationManager.notify(notificationId, builder.build());

        NotificationTable notificationTable = new NotificationTable(
                data.get("inner_title"),
                data.get("message"),
                String.valueOf(timestamp)
        );
        notificationTable.save();

    }
}
