package polines.monitoringtugasakhir.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import polines.monitoringtugasakhir.R;

public class CreateDialog {

    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;

    public void showAlertDialog(final Activity activity, final String title, final String message){

        alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OKE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
        alertDialog.show();

        final Button centerButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        LinearLayout.LayoutParams centerButtonLL = (LinearLayout.LayoutParams) centerButton.getLayoutParams();
        centerButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
        centerButton.setLayoutParams(centerButtonLL);

    }

    public void closeAlertDialog(){
        alertDialog.dismiss();
    }

    public void showProgressDialog(Activity activity, String title, String message){
        progressDialog = ProgressDialog.show(activity, title, message, true);
        progressDialog.getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }, 10000);
    }

    public void closeProgressDialog(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }, 500);
    }
}
