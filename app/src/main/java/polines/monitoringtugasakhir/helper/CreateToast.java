package polines.monitoringtugasakhir.helper;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import polines.monitoringtugasakhir.R;

public class CreateToast {
    Context mContext;

    public CreateToast(Context mContext) {
        this.mContext = mContext;
    }

    public void success(String message){
        Toasty.success(mContext, message, Toast.LENGTH_SHORT, true).show();
    }

    public void warning(String message){
        Toasty.warning(mContext, message, Toast.LENGTH_SHORT, true).show();
    }

    public void error(String message){
        Toasty.error(mContext, message, Toast.LENGTH_SHORT, true).show();
    }

    public void internalServerError(){
        Toasty.error(mContext, mContext.getString(R.string.internal_server_error), Toast.LENGTH_SHORT, true).show();
    }

    public void onFailure(){
        Toasty.error(mContext, mContext.getString(R.string.connection_failure), Toast.LENGTH_SHORT, true).show();
    }
}
